if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin réponse")

# ---- fiche question
from sympy import *
from random import sample,randint,randrange,choice

listeEnoncés = [
    ["Un sac contient [n] boules numérotés. On tire une boule au hasard.",
     "V", "La boule [n']est [pas ]verte.",
     "I", "La boule [n']a [pas ]un numéro impair."],
    ["Il y a [n] voitures sur un parking. On choisit une voiture au hasard.",
     "F", "La voiture [n']est [pas ]de marque française.",
     "B", "La voiture [n']est [pas ]blanche."]
    ]

exo = choice(listeEnoncés)

nAB = randint(5,25)
nA = randint(30,70)
nB = randint(30,70)
réunion = nA+nB-nAB
N = randint(réunion//10+1,15)*10
    
# q :	bit 0 : A ou A̅
#		bit 1 : B ou B̅
#		bit 2-3 : inter - union – condition 1 sachant 0 — condition 2 sachant 1

q = randrange(16)

def phrase(d,neg=False):
	if neg :
		d = d.replace("[","")
		d = d.replace("]","")
	else :
		d = d.replace("[ne ]","")
		d = d.replace("[n']","")
		d = d.replace("[pas ]","")
	return d

def phrase2(d1,d2,c):
	d1 = chr(ord(d2[0])|32) + d1[1:]
	d = d1[:-1]
	d = d+" "+c+" "
	d2 = chr(ord(d2[0])|32) + d2[1:]
	d = d+d2
	return d
    
ev1 = exo[1]
if q&0b1 :
    ev1 = r"{\overline "+ev1+r"}"
    
ev2 = exo[3]
if q&0b10 :
    ev2 = r"{\overline "+ev2+r"}"
celN = ",cell{"+str(2+(q//2)%2)+"}{"+str(2+q%2)+"}={orange!25!white}"
if q&0b1100==0b0000:
    p = "p("+ev1+r" \cap "+ev2+")"
    description = phrase2(phrase(exo[2],q&1),phrase(exo[4],q&2),"et que")
    celRef="{4}{4}"
elif q&0b1100==0b0100:
    p = "p("+ev1+r" \cup "+ev2+")"
    description = phrase2(phrase(exo[2],q&1),phrase(exo[4],q&2),"ou que")
    celN = ",cell{"+str(2+(q//2)%2)+"}{2-4}={orange!25!white}"
    celN = celN+",cell{2-4}{"+str(2+q%2)+"}={orange!25!white}"
    celRef="{4}{4}"
elif q&0b1100==0b1000:
    p = "p_"+ev1+r"("+ev2+")"
    description = phrase2(phrase(exo[4],q&2),phrase(exo[2],q&1),"sachant que")
    celRef="{4}{"+str(2+q%2)+"}"
elif q&0b1100==0b1100:
    p = "p_"+ev2+r"("+ev1+")"
    description = phrase2(phrase(exo[2],q&1),phrase(exo[4],q&2),"sachant que")
    celRef="{"+str(2+(q//2)%2)+"}{4}"
    

debutQflash("tableau à double entrée")
print(exo[0].replace("[n]",str(N)),end=r"\\")
print(r"On considère les événements :\\")
print(exo[1],r": \og",phrase(exo[2]),end=r"\fg \\")
print(exo[3],r": \og",phrase(exo[4]),end=r"\fg \\")
print(r"\begin{center}")
print(r"\begin{tblr}{hlines,vlines,colspec={cccc},row{1}={gray8,font=\bfseries},column1={gray8,font=\bfseries},rows={3ex},columns={5em}}")
print(r"Effectifs  & $",exo[1],r"$ & $\overline",exo[1],r"$ & Total \\")
print(r"$",exo[3],r"$ & ",nAB,r"&",nB-nAB,r"&",nB,r"\\")
print(r"$\overline",exo[3],r"$ & ",nA-nAB,r"&",N-réunion,r"&",N-nB,r"\\")
print(r"Total &",nA,r"&",N-nA,r"&",N,r"\\")
print(r"\end{tblr}")
print(r"\end{center}")
print(r"Décrire puis donner la probabilité $",p,"$")
finQflash()


# ---- fiche réponse
debutRflash()
print(r"$",p,"$ est la probabilité que ",description,r"\\")
print(r"\begin{center}")
print(r"\begin{tblr}{hlines,vlines,colspec={cccc},row{1}={gray8,font=\bfseries},column1={gray8,font=\bfseries},rows={3ex},columns={5em}",end="")
print(celN,end="")
print(r",cell"+celRef+"={orange!50!white}}")
desA="$"+exo[1]+"$"
desnA=r"$\overline "+exo[1]+"$"
if q&1:
    desnA=r"\tikz [overlay, baseline=(X.base)] \node [shape=circle, draw=red, line width=1.2pt, inner sep=2pt, text height=1.5ex,text depth=.25ex] (X) {"+desnA+"};"
else:
    desA=r"\tikz [overlay, baseline=(X.base)] \node [shape=circle, draw=red, line width=1.2pt, inner sep=2pt, text height=1.5ex,text depth=.25ex] (X) {"+desA+"};"
print(r"Effectifs  &",desA,r"&",desnA,r"& Total \\")
desB="$"+exo[3]+"$"
desnB=r"$\overline "+exo[3]+"$"
if q&2:
    desnB=r"\tikz [overlay, baseline=(X.base)] \node [shape=circle, draw=red, line width=1.2pt, inner sep=2pt, text height=1.5ex,text depth=.25ex] (X) {"+desnB+"};"
else:
    desB=r"\tikz [overlay, baseline=(X.base)] \node [shape=circle, draw=red, line width=1.2pt, inner sep=2pt, text height=1.5ex,text depth=.25ex] (X) {"+desB+"};"
print(desB,r"&",nAB,r"&",nB-nAB,r"&",nB,r"\\")
print(desnB,r"&",nA-nAB,r"&",N-réunion,r"&",N-nB,r"\\")
print(r"Total &",nA,r"&",N-nA,r"&",N,r"\\")
print(r"\end{tblr}")
print(r"\end{center}")
print("$"+p+"=",end="")
if q&0b1100==0b0000:
    print(r"\dfrac{n("+ev1+r" \cap "+ev2+")}{N}=",end="")
    print("\dfrac{",[nAB,nB-nAB,nA-nAB,N-réunion][q%4],"}{",N,"}",end="")
elif q&0b1100==0b0100:
    print(r"\dfrac{n("+ev1+")+n("+ev2+")-n("+ev1+r" \cap "+ev2+")}{N}=",end="")
    print("\dfrac{",[nA,N-nA][q%2],"+",[nB,N-nB][(q//2)%2],"-",[nAB,nB-nAB,nA-nAB,N-réunion][q%4],"}{",N,"}=",end="")
    print("\dfrac{",[nA,N-nA][q%2]+[nB,N-nB][(q//2)%2]-[nAB,nB-nAB,nA-nAB,N-réunion][q%4],"}{",N,"}",end="")
elif q&0b1100==0b1000:
    print(r"\dfrac{n("+ev1+r" \cap "+ev2+")}{n("+ev1+")}=",end="")
    print("\dfrac{",[nAB,nB-nAB,nA-nAB,N-réunion][q%4],"}{",[nA,N-nA][q%2],"}",end="")
elif q&0b1100==0b1100:
    print(r"\dfrac{n("+ev1+r" \cap "+ev2+")}{n("+ev2+")}=",end="")
    print("\dfrac{",[nAB,nB-nAB,nA-nAB,N-réunion][q%4],"}{",[nB,N-nB][(q//2)%2],"}",end="")
print("$")
finRflash()
