#----
#
# \usepackage[tikz]
# \usetikzlibrary{positioning}
#
#----
if __name__ == "__main__":
    settings = dict(canonique=True)

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ----
from sympy import *
from random import choice, randint, randrange

x = symbols('x')

if 'choix' in settings:
    choix = settings['choix']
else:
    choix = randrange(3)

yM = 0


if choix==0 : # forme développée
    if 'a' in settings:
        a = settings['a']
    else :
        a = choice([-3,-2,-1,1,2,3])


    if 'b' in settings:
        b = settings['b']
    else :
        b = choice([-3,-2,-1,1,2,3])


    if 'c' in settings:
        c = settings['c']
    else :
        c = randint(-3,3)

    alpha = Rational(-b,2*a)
    beta = (a*alpha+b)*alpha+c
    
    xM = 1
    yM = a+b+c
elif choix==1 : # forme canonique
    if 'alpha' in settings :
        alpha = settings['alpha']
    else:
        alpha = choice([-3,-2,-1,1,2,3])
        
    if 'beta' in settings :
        beta = settings['beta']
    else:
        beta = choice([-3,-2,-1,1,2,3])
        
    if 'M' in settings :
        xM, yM = settings['M']
    else:
        xM = alpha
        while xM==alpha:
            xM = choice([-3,-2,-1,1,2,3])
            
        if 'a' in settings:
            a = settings['a']
            yM = a*(xM-alpha)**2+beta
        else:
            yM = beta + choice([-3,-2,-1,1,2,3])
            
    a = Rational(yM-beta,(xM-alpha)**2)
    b = -2*alpha*a
    c = a*alpha**2+beta
else: # forme factorisée
    if 'x1' in settings :
        x1 = settings['x1']
    else:
        x1 = choice([-3,-2,-1,1,2,3])
        
    if 'x2' in settings :
        x2 = settings['x2']
    else:
        x2 = choice([-3,-2,-1,1,2,3])
        
    while x1==x2:
        x2 = choice([-3,-2,-1,1,2,3])
        
    alpha = Rational(x1+x2,2)
    
    if 'M' in settings :
        xM, yM = settings['M']
    else:
        xM = choice([-3,-2,-1,1,2,3])
        
        while 2*xM in [2*x1,2*x2,(x1+x2)]:
            xM = choice([-3,-2,-1,1,2,3])

        if 'a' in settings:
            a = settings['a']
            yM = a*(xM-x1)*(xM-x2)
        else:
            yM = choice([-3,-2,-1,1,2,3]) 

    a = Rational(yM,(xM-x1)*(xM-x2))
    b = -a*(x1+x2)
    beta = a*(alpha-x1)*(alpha-x2)
    c = a*x1*x2

if choix==0:
    ymax = int(max([0,beta+0.7,c,c+b,c+b+a,yM])+1)
    ymin = int(min([0,beta-0.7,c,c+b,c+b+a,yM])-1)
else :
    ymax = int(max([0,beta+0.7,yM])+1)
    ymin = int(min([0,beta-0.7,yM])-1)

if ymax-ymin>5:
    yscl = 5/(ymax-ymin)
else :
    yscl = 1

if a==1:
    fa=''
elif a==-1:
    fa='-'
else:
    fa=latex(a)+r'\,'
    
# ---- fiche question
debutQflash("\small Déterminer graphiquement une expression")
print(r'Trouver la fonction polynomiale donc la représentation graphique est la parabole')
print(r'\begin{center}')
print(r'\begin{tikzpicture}[yscale='+str(yscl)+']')
print(r'\tkzInit[xmin=-4,xmax=4,xstep=1,ymin='+str(ymin)+',ymax='+str(ymax)+',ystep=1]')
print(r'\tkzGrid')
print(r'\tkzAxeXY[font=\small,fill=none]')
if choix==0: # forme développé
    print(r'\fill[green!50!black] (0,'+str(c)+r') circle [radius=2pt];')
    print(r'\tkzFct[orange!80!black,line width=1pt, domain=-4:4]{'+str(float(b))+'*x+'+str(float(c))+'})')
elif choix==1: # forme canonique
    print(r'\fill[green!50!black] ('+str(xM)+r','+str(yM)+r') circle [radius=2pt];')
    if a>0:
        print(r'\fill[green!50!black] ('+str(alpha)+r','+str(beta)+r') circle [radius=2pt] node [below right] {$S$};')
    else:
        print(r'\fill[green!50!black] ('+str(alpha)+r','+str(beta)+r') circle [radius=2pt] node [above right] {$S$};')
else: # forme factorisée
    print(r'\fill[green!50!black] ('+str(xM)+r','+str(yM)+r') circle [radius=2pt];')
    print(r'\fill[green!50!black] ('+str(x1)+r',0) circle [radius=2pt];')
    print(r'\fill[green!50!black] ('+str(x2)+r',0) circle [radius=2pt];')
    
print(r'\tkzFct[green!50!black,line width=1pt, domain=-4:4]{('+str(float(a))+'*x+'+str(float(b))+')*x+'+str(float(c))+'})')
print(r'\end{tikzpicture}')
print(r'\end{center}')
if choix==1: # forme canonique
    print(r'où $S$ est le sommet de cette parabole')
finQflash()

# ---- fiche réponse
debutRflash()
if choix==0: # forme développé
    print(r'\[ f \colon x \mapsto ',latex(a*x**2+b*x+c),r'\]')
elif choix==1: # forme canonique
    print(r'\[ f \colon x \mapsto ',latex(a*(x-alpha)**2+beta),r'\]')
else: # forme factorisée
    print(r'\[ f \colon x \mapsto ',fa+latex((x-x1)*(x-x2)),r'\]')

if choix==1: # forme canonique
    print(r"\begin{minipage}[c]{.55\linewidth}")
    print(r"On a le sommet $S$ de la parabole\\")
    print(r'donc une forme $a\,(\begin{tikzpicture}[baseline=(E.base)] \node (E) {$'+latex(x-alpha)+r"$};")
    print(r"\draw [orange,decorate,decoration=brace,thick] (E.south east) -- node [below,font=\tiny,align=center] {s'annule\\ en $"+latex(alpha)+r"$}(E.south west);")
    if beta>=0:
        sbeta="+"+str(beta)
    else:
        sbeta=str(beta)
    print(r"\end{tikzpicture})^2"+sbeta+r"$\\")
    print(r"avec le point $M(",xM,";",yM,r")$ on a\\")
    print(r"$a\times (",latex(x-alpha).replace("x",str(xM)),r")^2 "+sbeta+" =",yM,r"$\\")
    print(r"d'où $a=",latex(a),r"$")
    print(r"\end{minipage}")
elif choix==2: # forme factorisé
    print(r"\begin{minipage}[c]{.55\linewidth}")
    print(r"On a deux racines\\")
    print(r'donc une forme $a\,(\begin{tikzpicture}[baseline=(E.base)] \node (E) {$'+latex(x-x1)+r"$};")
    print(r"\draw [orange,decorate,decoration=brace,thick] (E.south east) -- node [below,font=\tiny,align=center] {s'annule\\ en $"+latex(x1)+r"$}(E.south west);")
    print(r'\end{tikzpicture})(\begin{tikzpicture}[baseline=(E.base)] \node (E) {$'+latex(x-x2)+r"$};")
    print(r"\draw [orange,decorate,decoration=brace,thick] (E.south east) -- node [below,font=\tiny,align=center] {s'annule\\ en $"+latex(x2)+r"$}(E.south west);")
    print(r"\end{tikzpicture})$\\")
    print(r"avec le point $M(",xM,";",yM,r")$ on a\\")
    print(r"$a\times (",latex(x-x1).replace("x",str(xM)),r")\times (",latex(x-x2).replace("x",str(xM)),r")=",yM,r"$\\")
    print(r"d'où $a=",latex(a),r"$\\")
    print(r"\end{minipage}")

if choix==0:
    print(r'\begin{center}')
    print(r'\begin{tikzpicture}[yscale='+str(yscl)+']')
else:
    print(r"\begin{minipage}[c]{.43\linewidth}")
    print(r'\begin{tikzpicture}[yscale='+str(yscl)+r',xscale=.5]')
print(r'\tkzInit[xmin=-4,xmax=4,xstep=1,ymin='+str(ymin)+',ymax='+str(ymax)+',ystep=1]')
print(r'\tkzGrid')
print(r'\tkzAxeXY[font=\small,fill=none]')
if a>0:
    pos = "below "
else:
    pos = "above"
if xM>alpha:
    pos += " right"
else:
    pos += " left"
print(r'\fill[green!50!black] ('+str(xM)+r','+str(yM)+r') circle [radius=2pt] node ['+pos+'] {$M$};')
if choix==0: # forme développé
    print(r'\fill[green!50!black] (0,'+str(c)+r') circle [radius=2pt];')
    print(r'\tkzFct[orange!90!black,line width=1pt, domain=-4:4]{'+str(float(b))+'*x+'+str(float(c))+'})')
elif choix==1: # forme canonique
    print(r'\draw [red,thick] ('+str(alpha)+r',0) -- ('+str(alpha)+r','+str(beta)+r') -- (0,'+str(beta)+r');')
    if a>0:
        print(r'\fill[green!50!black] ('+str(alpha)+r','+str(beta)+r') circle [radius=2pt] node [below right] {$S$};')
    else:
        print(r'\fill[green!50!black] ('+str(alpha)+r','+str(beta)+r') circle [radius=2pt] node [above right] {$S$};')
else: # forme factorisée
    print(r'\fill[green!50!black] ('+str(x1)+r',0) circle [radius=2pt];')
    print(r'\fill[green!50!black] ('+str(x2)+r',0) circle [radius=2pt];')
print(r'\tkzFct[green!50!black,line width=1pt, domain=-4:4]{('+str(float(a))+'*x+'+str(float(b))+')*x+'+str(float(c))+'})')
if choix==0: # forme développé
    print(r'\node[left=2pt,fill=orange!20,rounded corners] at (0,'+str(c)+r') {\color{orange} \tiny $c='+str(c)+'$};')
    if b>0:
        pos='below'
    else:
        pos='above'
    print(r'\draw [orange,-latex, very thick] (0,'+str(c)+r') -- node ['+pos+r'] {$1$} ++(1,0) -- node [right=2pt,fill=orange!20,rounded corners] {\tiny $b='+str(b)+'$} ++(0,'+str(b)+r');')
    if a*b>0: #même sens
        pos='1'
    else:
        pos='0.94'
    print(r'\draw [red,-latex, very thick] ('+pos+','+str(c+b)+r') -- node [left=2pt,fill=red!20,rounded corners] {\tiny $a='+str(a)+'$} ++(0,'+str(a)+r');')
elif choix==1: # forme canonique
    if beta>0:
        pos='below'
    else:
        pos='above'
    print(r'\node['+pos+r'=2pt,fill=red!20,rounded corners] at ('+str(alpha)+r',0) {\color{red} \tiny $\alpha='+str(alpha)+'$};')
    if alpha>0:
        pos='left'
    else:
        pos='right'
    print(r'\node['+pos+r'=2pt,fill=red!20,rounded corners] at (0,'+str(beta)+r') {\color{red} \tiny $\beta='+str(beta)+'$};')
else: # forme factorisée
    print(r'\node[below=2pt,fill=green!20,rounded corners] at ('+str(x1)+r',0) {\color{green!50!black} \tiny $x_1='+str(x1)+'$};')
    print(r'\node[below=2pt,fill=green!20,rounded corners] at ('+str(x2)+r',0) {\color{green!50!black} \tiny $x_2='+str(x2)+'$};')
print(r'\end{tikzpicture}')
if choix==0:
    print(r'\end{center}')
else:
    print(r"\end{minipage}")
finRflash()
