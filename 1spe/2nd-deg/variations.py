#----
#
# \usepackage[tikz]
# \usetikzlibrary{positioning}
#
#----
if __name__ == "__main__":
    settings = dict(canonique=True)

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ----
from sympy import *
from random import choice, randint

x = symbols('x')
if settings!=None and 'a' in settings:
    a = settings['a']
else :
    a = choice([-3,-2,-1,1,2,3])

alpha = None
c = None

if settings!=None and 'b' in settings:
    b = settings['b']
elif settings!=None and ('alpha' in settings or 'beta' in settings):
    if 'alpha' in settings :
        alpha = settings['alpha']
        if 'beta' in settings :
            beta = settings['beta']
        else :
            beta = randint(-5,5)
    else :
        alpha = randint(-5,5)
        beta = settings['beta']
    b = -2*a*alpha
    c = a*alpha**2+beta
else :
    b = randint(-5,5)

if c==None and settings!=None and 'c' in settings:
    c = settings['c']
elif c==None:
    c = randint(-3,3)

if alpha==None :
    alpha = Rational(-b,2*a)
    beta = (a*alpha+b)*alpha+c

# ---- fiche question
debutQflash("Variations")
if settings!=None and 'canonique' in settings and settings['canonique']:
    E = latex(a*(x-alpha)**2+beta)
else:
    E = latex(a*x**2+b*x+c)

print(r'Déterminer le tableau de variations de la fonction $f$ définie sur $\mathbb R$ par')
print(r'\[f \colon x \mapsto',E,r'\]')

finQflash()

# ---- fiche réponse
debutRflash()
if settings!=None and'canonique' in settings and settings['canonique']:
    print('$',E,r"$ est une forme canonique du second degré où\\")
    print(r'\[a=',a,r'\quad \alpha=',latex(alpha),r'\text{ et } \beta=',latex(beta),r'\]')
else:
    print('$',E,r"$ est une forme développée du second degré où\\")
    print(r'\[a=',a,r'\quad b=',latex(b),r'\text{ et } c=',latex(c),r'\]')
    print(r'on calcule\\')
    print(r'$\displaystyle \alpha=-\frac{b}{2a}=',latex(alpha),r'$\\')
    print(r'$\displaystyle \beta=f(\alpha)=f\left(',latex(alpha),r'\right)=',latex(beta),r'$\\')
print()

if a>0:
    print(r'On en déduit, comme $a>0$\\')
else:
    print(r'On en déduit, comme $a<0$\\')
        
print(r'\begin{center}')
print(r'\begin{tikzpicture}')
print(r'\tikzset{node style/.style = {inner sep = 2pt, outer sep = 2pt, fill = none}}')
print(r'\tkzTabInit [lgt=2.5,espcl=2] {$x$ / .7 ,variations\\de $f$ /1.5 }{$-\infty$,$\alpha=',latex(alpha),r'$,$+\infty$}')

if a>0:
    print(r'\tkzTabVar{+/$+\infty$,-/$\beta=',latex(beta),r'$,+/$+\infty$}')
else:
    print(r'\tkzTabVar{-/$-\infty$,+/$\beta=',latex(beta),r'$,-/$-\infty$}')

print(r'\end{tikzpicture}')
print(r'\end{center}')

finRflash()
