#----
#
# \usepackage[tikz]
# \usetikzlibrary{positioning}
#
#----
if __name__ == "__main__":
    settings = dict(a=3,alpha=-1,delta=2)

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ----
from sympy import *
from random import choice, randint

x = symbols('x')
if 'a' in settings:
    a = settings['a']
else :
    a = choice([-3,-2,-1,1,2,3])
    
if 'factorise' in settings:
    factorise = settings['factorise']
else :
    factorise = False # par défaut


if 'alpha' in settings:
    alpha = settings['alpha']
    if 'delta' in settings :
        x1 = alpha-sqrt(settings['delta'])
        x2 = alpha+sqrt(settings['delta'])
        c = a*(alpha**2-settings['delta'])
    else:
        x1 = alpha
        x2 = alpha
        c = a*alpha**2
    # f = a*(x-x1)*(x-x2)
    b = -a*2*alpha
elif 'x1' in settings:
    x1 = settings['x1']
    if 'x2' in settings:
        x2 = settings['x2']
    else:
        x2 = randint(-5,5)
    b = -a*(x1+x2)
    c = a*x1*x2
else :
    if 'b' in settings :
        b = settings['b']
    else:
        b = randint(-5,5)
        
    if 'c' in settings :
        c = settings['c']
    else:
        c = randint(-5,5)
    
Delta = b**2-4*a*c
alpha = Rational(-b,2*a)
if Delta>=0:
    x1 = (-b-sqrt(Delta))*Rational(1,2*a)
    x2 = (-b+sqrt(Delta))*Rational(1,2*a)

if a==1:
    fa=''
elif a==-1:
    fa='-'
else:
    fa=str(a)+r'\,'

if factorise:
    if x1==x2:
        E = fa+'('+latex(x-x1)+')^2'
    else:
        E = fa+'('+latex(x-x1)+')('+latex(x-x2)+')'
else:
    E =latex(a*x**2+b*x+c)
    


# ---- fiche question
debutQflash("Signe")

    
print(r'Déterminer le tableau de signe de $f(x)$ où la fonction $f$ est définie sur $\mathbb R$ par')
print(r'\[f \colon x \mapsto',E,r'\]')

finQflash()

# ---- fiche réponse
debutRflash()
if factorise and x1==x2:
    print('$'+fa+r'(\begin{tikzpicture}[baseline=(E.base)] \node (E) {$'+latex(x-alpha)+r"$};")
    print(r"\draw [orange,decorate,decoration=brace,thick] (E.south east) -- node [below,font=\tiny,align=center] {s'annule\\ en $"+latex(alpha)+r"$}(E.south west);")
    print(r"\end{tikzpicture})^2$ est une forme factorisée où")
    print(r'\[a='+latex(a)+r'\quad x_1=x_2=\alpha=',latex(alpha),r'\]')
    print("on a donc une racine double")
elif factorise :
    print('$'+fa+r'(\begin{tikzpicture}[baseline=(E.base)] \node (E) {$'+latex(x-x1)+r"$};")
    print(r"\draw [orange,decorate,decoration=brace,thick] (E.south east) -- node [below,font=\tiny,align=center] {s'annule\\ en $"+latex(x1)+r"$}(E.south west);")
    print(r'\end{tikzpicture})(\begin{tikzpicture}[baseline=(E.base)] \node (E) {$'+latex(x-x2)+r"$};")
    print(r"\draw [orange,decorate,decoration=brace,thick] (E.south east) -- node [below,font=\tiny,align=center] {s'annule\\ en $"+latex(x2)+r"$}(E.south west);")
    print(r"\end{tikzpicture})$ est une forme factorisée où")
    print(r'\[a='+latex(a)+r'\quad x_1='+latex(x1)+r'\quad x_2='+latex(x2)+r'\]')
else:
    print('$',E,r"$ est une forme développée du second degré où\\")
    print(r'\[a=',a,r'\quad b=',latex(b),r'\text{ et } c=',latex(c),r'\]')
    print(r'on calcule\\')
    print(r'$\Delta=b^2-4\,a\,c=',latex(Delta),r'$\\')
    if Delta<0:
        print(r"Comme $\Delta<0$, on n'a pas de racine.\\")
    elif Delta==0:
        print(r"Comme $\Delta=0$, on a une racine double\\")
        print(r'$x1=x2=\alpha=',latex(alpha),r'$\\')
    else:
        print(r"Comme $\Delta>0$, on a deux racines\\")
        print(r'$\displaystyle x_1=\frac{-b-\sqrt{\Delta}}{2\,a}='+latex(x1)+r'$\\')
        print(r'$\displaystyle x_2=\frac{-b+\sqrt{\Delta}}{2\,a}='+latex(x2)+r'$\\')

print()
if a>0:
    print(r'On en déduit, comme $a>0$')
    s="+"
    ms="-"
else:
    print(r'On en déduit, comme $a<0$')
    s="-"
    ms="+"
    
print(r'\begin{center}')
print(r'\begin{tikzpicture}')
print(r'\tikzset{node style/.style = {inner sep = 2pt, outer sep = 2pt, fill = none}}')
if Delta<0:
    print(r'\tkzTabInit [lgt=2.5,espcl=2] {$x$ / .7 ,signe\\de $f(x)$ /1 }{$-\infty$,$+\infty$}')
    print(r'\tkzTabLine{,'+s+r',};')
elif Delta==0:
    print(r'\tkzTabInit [lgt=2.5,espcl=2] {$x$ / .7 ,signe\\de $f(x)$ /1 }{$-\infty$,$\alpha=',latex(alpha),r'$,$+\infty$}')
    print(r'\tkzTabLine{,'+s+r',z,'+s+',};')
else:
    if x1>x2:
        X1='x_2='+latex(x2)
        X2='x_1='+latex(x1)
    else:
        X1='x_1='+latex(x1)
        X2='x_2='+latex(x2)
    print(r'\tkzTabInit [lgt=2,espcl=2.5] {$x$ / .7 ,signe\\de $f(x)$ /1 }{$-\infty$,$'+X1+'$,$'+X2+r'$,$+\infty$}')
    print(r'\tkzTabLine{,'+s+r',z,'+ms+',z,'+s+',};')
print(r'\end{tikzpicture}')
print(r'\end{center}')

finRflash()
