#----
#
# Les "columns" sont intégrées à la class beamer
#
# \usepackage[tikz]
#
#----
if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#----
from sympy import *
from random import *

if random()<.1 :
    a = randint(-10,9)*pi/2
else :
    a = choice([-1,1])*choice([pi/3,pi/4,pi/6])+randint(-30,30)*pi/2
if -pi < a <= pi :
    a = a +2*randint(3,10)*choice([-1,1])*pi
k = floor(a/(2*pi)+.5)
b = a-2*k*pi        
    
#---- fiche question
debutQflash("Mesure principale")
print(r"Quelle est la mesure principale de l'angle $\displaystyle \alpha = "+latex(a)+r"$ ?\\")
print()
print(r"En déduire les valeurs de $\cos \alpha$ et de $\sin \alpha$.")
finQflash()

#---- fiche réponse
debutRflash()
print(r"""\begin{columns}
\column{.6\textwidth}
On a $\displaystyle  """+latex(a)+" = "+latex(b),end='')
if k>0:
    print("+"+str(k),end='')
else:
    print(k,end='')
print(r"\times 2\, \pi$\\")
if a/pi != int(a/pi):
    sa = str(int(100*a/pi)/100).replace('.',',')
    if 4*a/pi != int(4*a/pi):
        print("( $"+latex(a/pi)+r" \approx "+sa+"$ ",end='')
    else:
        print("( $"+latex(a/pi)+r" = "+sa+"$ ",end='')
    print("donc proche de $"+str(k)+r"\times 2$ )\\")
    

print(r"\medskip")
print(r"donc la mesure principale de $\alpha$ est $"+latex(b)+r"$.\\")
print(r"\medskip")
print(r"$\cos \alpha ="+latex(cos(a))+r"$\\")
print(r"$\sin \alpha ="+latex(sin(a))+r"$\\")

print(r"""\column{.4\textwidth}
\begin{tikzpicture}[scale=1.7]
\draw [thick,-latex] (-1.1,0) -- (1.2,0) node [below] {\color{blue} cos};
\draw [thick,-latex] (0,-1.1) -- (0,1.2) node [left] {\color{green!50!black} sin};
\draw [thick] (0,0) circle [radius=1];""")

if sin(a)>0:
    pos = "below"
else:
    pos = "above"
print(r"\draw [blue, dashed] ("+str(int(b*180/pi))+":1) -- ("+str(float(cos(a)))+r",0) node ["+pos+r"] {$"+latex(cos(a))+r"$};")

if cos(a)>0:
    pos = "left"
else:
    pos = "right"
print(r"\draw [green!50!black, dashed] ("+str(int(b*180/pi))+r":1) -- (0,"+str(float(sin(a)))+r",0) node ["+pos+r"] {$"+latex(sin(a))+r"$};")

print(r"\filldraw [orange, thick] (0,0) -- ("+str(int(b*180/pi))+r":1) circle [radius=1pt] node [label="+str(int(b*180/pi))+r":$"+latex(b)+r"""$] {};
\end{tikzpicture}
\end{columns}""")
finRflash()