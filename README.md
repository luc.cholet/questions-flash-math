# Questions Flash Math

Petit projet pour la création de PDF avec des questions flash en math.


## code LuaLaTeX

J'utilise le package `luapython` pour l'intégration du *python* dans LaTeX donc il faut compiler avec `lualatex -shell-escape`.

## modules pythons
De petits codes *python* génèrent des codes LaTeX qui seront ensuite inclus dans un fichier `beamer`. Les modules sont indépendants et peuvent être exécutés séparément. Le code fourni peut ensuite être copié-collé directement de la console *python* vers un fichier *LaTeX*.

## catalogue
Les fichiers `catalogue.pdf`et `catalogue.tex` fournissent un petit catalogue des modules pour l'instant proposés.