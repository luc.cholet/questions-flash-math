if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ---- fiche question
from random import randint

p1 = randint(1,19)*5
if p1%2==0 :
    if p1%4==0:
        p2 = randint(1,19)*5
    else:
        p2 = randint(1,9)*10
else:
    p2 = randint(1,4)*20
pg = p1*p2//100

c = randint(1,3)

debutQflash("proportions échelonnées")
print(r"""$A \subset B \subset C$\\
\begin{tikzpicture}[overlay, xshift=8cm, yshift=2ex,
 ensA/.style={ellipse,draw,inner sep=0pt,minimum width=8mm, minimum height=6mm},
 ensB/.style={ellipse,draw,inner sep=0pt,minimum width=16mm, minimum height=9mm},
 ensC/.style={rectangle, rounded corners=2pt,draw,inner sep=0pt, minimum width=2cm, minimum height=1.2cm}]
\node [ensC] at (0,0) (O1) {};
\node at (-1.5,-.5) (O2) {\small $C$};
\draw (O1) -- (O2);

\node [ensA] at (.2,-.1) (A1) {};
\node at (1.5,.5) (A2) {\small $A$};
\draw (A1) -- (A2);

\node [ensB] at (.1,0) (B1) {};
\node at (1.6,-.5) (B2) {\small $B$};
\draw (B1) -- (B2);
\end{tikzpicture}\\""")

if c!=1 :
    print(r"La part de $A$ dans $B$ est de ",p1,r"~\%.\\",sep="")
if c!=2 :
    print(r"La part de $B$ dans $C$ est de ",p2,r"~\%.\\",sep="")
if c!=3 :
    print(r"La part de $A$ dans $C$ est de ",pg,r"~\%.\\",sep="")
    
if c==1 :
    print(r"Quelle est la part de $A$ dans $B$ ?")
if c==2 :
    print(r"Quelle est la part de $B$ dans $C$ ?")
if c==3 :
    print(r"Quelle est la part de $A$ dans $C$ ?")
    

finQflash()

# ---- fiche réponse
debutRflash()
if c!=1 :
    print(r"La part de $A$ dans $B$ est de ",p1,r"~\%.\\",sep="")
if c!=2 :
    print(r"La part de $B$ dans $C$ est de ",p2,r"~\%.\\",sep="")
if c!=3 :
    print(r"La part de $A$ dans $C$ est de ",pg,r"~\%.\\",sep="")
print(r"""\begin{center}
\begin{tikzpicture}[baseline = (A.base),scale=2, line width=1.5pt,
	val/.style={inner sep=1.5mm,node distance=2cm, text height=1.5ex,text depth=.25ex, circle, minimum size=6mm,rounded corners=3mm,draw=black,fill=black!10!white}]
\node (C) [val] {$C$};
\node (B) [val, right=of C] {$B$};
\node (A) [val, right=of B] {$A$};""")

if c==1 :
    print(r"\path (B) edge [-latex,bend left, red] node [below]{\footnotesize $\times p_2$} node [above]{\footnotesize ?} (A);")
else:
    print(r"\path (B) edge [-latex,bend left, green!75!black] node [below]{\footnotesize $\times "+str(p1/100).replace('.',','),r"$} node [above]{\footnotesize ",p1,r"~\%} (A);",sep="")
    
if c==2 :
    print(r"\path (C) edge [-latex,bend left, red] node [below]{\footnotesize $\times p_1$} node [above]{\footnotesize ?} (B);")
else:
    print(r"\path (C) edge [-latex,bend left, green!75!black] node [below]{\footnotesize $\times "+str(p2/100).replace('.',','),r"$} node [above]{\footnotesize ",p2,r"~\%} (B);",sep="")

if c==3 :
    print(r"\path (C) edge [-latex,bend right, red] node [below]{\footnotesize $\times p_\text{global}$} node [above]{\footnotesize ?} (A);")
else:
    print(r"\path (C) edge [-latex,bend right, green!75!black] node [below]{\footnotesize $\times "+str(pg/100).replace('.',','),r"$} node [above]{\footnotesize ",pg,r"~\%} (A);",sep="")

print(r"""\end{tikzpicture}
\end{center}""")

if c==1 :
    print(r"$"+str(p2/100).replace('.',',')+r"\times p_2 = "+str(pg/100).replace('.',',')+r"$\\")
    print(r"d'où $p_2 = \dfrac{"+str(pg/100).replace('.',',')+r"}{"+str(p2/100).replace('.',',')+r"} = "+str(p1/100).replace('.',',')+r"$\\")
    print(r"donc la part de $A$ dans $B$ est de ",p1,r"~\%.\\",sep="")
if c==2 :
    print(r"$p_1 \times"+str(p1/100).replace('.',',')+r" = "+str(pg/100).replace('.',',')+r"$\\")
    print(r"d'où $p_1 = \dfrac{"+str(pg/100).replace('.',',')+r"}{"+str(p1/100).replace('.',',')+r"} = "+str(p2/100).replace('.',',')+r"$\\")
    print(r"donc la part de $B$ dans $C$ est de ",p2,r"~\%.\\",sep="")
if c==3 :
    print(r"$p_\text{global}="+str(p2/100).replace('.',',')+r"\times "+str(p1/100).replace('.',',')+r" = "+str(pg/100).replace('.',',')+r"$\\")
    print(r"donc la part de $A$ dans $C$ est de ",pg,r"~\%.\\",sep="")
    
finRflash()
