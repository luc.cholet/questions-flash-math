if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ---- fiche question
from sympy import *
from random import sample,randint,randrange,choice

l = sample([3,5,7],2)
n1 = l[0]
n2 = l[1]

if choice([True, False]):
    pA = randint(1,19)*5
    if pA%2==0 :
        if pA%4==0:
            pB1 = randint(1,19)/20
            pB2 = randint(1,19)/20
        else:
            pB1 = randint(1,9)/10
            pB2 = randint(1,9)/10
    else:
        pB1 = randint(1,4)/5
        pB2 = randint(1,4)/5
    pA = pA/100
else:
    pA = Rational(randint(1,n1-1),n1)
    pB1 = Rational(randint(1,n2-1),n2)
    pB2 = Rational(randint(1,n2-1),n2)
    
c = randint(0,7) # le choix des 3 branches à afficher
q = randrange(6) # quelle proba calculer

lstQ = [r"p(B)", r"p(\overline B)", r"p(A \cap B)", r"p(A \cap \overline B)", r"p( \overline A \cap B)", r"p( \overline A \cap  \overline B)"]

debutQflash("arbre pondéré")
print("Compléter l'arbre pondéré")
print(r"\begin{center}")
print(r"""\tikzstyle{start} = [circle, minimum width=3pt,fill, inner sep=0pt]
\tikzstyle{bag} = [text width=1em, text centered]
\tikzstyle{end} = [text centered]""")
print(r"""\begin{tikzpicture}[%overlay,xshift=11cm,yshift=-.5cm,
 level 1/.style={level distance=2cm, sibling distance=12ex},
 level 2/.style={level distance=2cm, sibling distance=6ex},
 parent anchor=east,child anchor=west,grow'=east,sloped]

\node[start] {}
    child {
        node[bag] {$A$}        
        child {
		node[bag] {$B$}
		edge from parent [->,""",end="")
if c&0b010:
    print(r"""black]
		node[above] {\small $\times """+latex(pB1).replace('.',',')+r"$",end="")
else:
    print(r"""red]
		node[above] {\small \dots{}""",end="")
print(r"""}
            }
            child {
		node[bag] {$\overline{B}$}
		edge from parent [->,""",end="")
if not c&0b010:
    print(r"""black]
 		node[below] {\small  $\times """+latex(1-pB1).replace('.',',')+r"$",end="")
else:
    print(r"""red]
 		node[below] {\small  \dots{}""",end="")
print(r"""}
            }
            edge from parent  [parent anchor=base,->,""",end="")
if c&0b001:
    print(r"""black]
		node[above] {\small $\times """+latex(pA).replace('.',',')+r"$",end="")
else:
    print(r"""red]
		node[above] {\small \dots{}""",end="")
print(r"""}
     }
    child {
        node[bag] {$\overline{A}$}        
        child {
		node[bag] {$B$}
		edge from parent [->,""",end="")
if c&0b100:
    print(r"""black]
		node[above] {\small $\times """+latex(pB2).replace('.',',')+r"$",end="")
else:
    print(r"""red]
		node[above] {\small \dots{}""",end="")
print(r"""}
            }
            child {
		node[bag] {$\overline{B}$}
		edge from parent [->,""",end="")
if not c&0b100:
    print(r"""black]
		node[below] {\small $\times """+latex(1-pB2).replace('.',',')+r"$",end="")
else:
    print(r"""red]
		node[below] {\small \dots{}""",end="")
print(r"""}
            }
        edge from parent [parent anchor=base,->,""",end="")
if not c&0b001:
    print(r"""black]      
		node[below] {\small $\times """+latex(1-pA).replace('.',',')+r"$",end="")
else:
    print(r"""red]      
		node[below] {\small \dots{}""",end="")
print(r"""}
    };
\end{tikzpicture}""")
print(r"\end{center}")
print(r"puis calculer $"+lstQ[q]+"$")
finQflash()

# ---- fiche réponse
debutRflash()
print(r"\begin{center}")
print(r"""\tikzstyle{start} = [circle, minimum width=3pt,fill, inner sep=0pt]
\tikzstyle{bag} = [text width=1em, text centered]
\tikzstyle{end} = [text centered, fill=blue!20!white, right=.7cm, shape=rectangle, rounded corners=5pt]""")
print(r"""\begin{tikzpicture}[%overlay,xshift=11cm,yshift=-.5cm,
 level 1/.style={level distance=2cm, sibling distance=12ex},
 level 2/.style={level distance=2cm, sibling distance=6ex},
 parent anchor=east,child anchor=west,grow'=east,sloped]

\node[start] {}
    child {
        node[bag] {$A$}        
        child {
		node[bag] {$B$}""")
if q in [0,2]:
    print(r"node[end] {$A \cap B$}")
print(r"""edge from parent [->,""",end="")
if c&0b010:
    print("black",end="")
else:
    print("red",end="")
print(r"""]
		node[above] {\small $\times """+latex(pB1).replace('.',',')+r"""$}
            }
            child {
		node[bag] {$\overline{B}$}""")
if q in [1,3]:
    print(r"node[end] {$A \cap \overline B$}")
print(r"""edge from parent [->,""",end="")
if not c&0b010:
    print("black",end="")
else:
    print("red",end="")
print(r"""]
 		node[below] {\small  $\times """+latex(1-pB1).replace('.',',')+r"""$}
            }
            edge from parent  [parent anchor=base,->,""",end="")
if c&0b001:
    print("black",end="")
else:
    print("red",end="")
print(r"""]
		node[above] {\small $\times """+latex(pA).replace('.',',')+r"""$}
     }
    child {
        node[bag] {$\overline{A}$}        
        child {
		node[bag] {$B$}""")
if q in [0,4]:
    print(r"node[end] {$\overline A \cap B$}")
print("""edge from parent [->,""",end="")
if c&0b100:
    print("black",end="")
else:
    print("red",end="")
print(r"""]
		node[above] {\small $\times """+latex(pB2).replace('.',',')+r"""$}
            }
            child {
		node[bag] {$\overline{B}$}""")
if q in [1,5]:
    print(r"node[end] {$\overline A \cap \overline B$}")
print("""edge from parent [->,""",end="")
if not c&0b100:
    print("black",end="")
else:
    print("red",end="")
print(r"""]
		node[below] {\small $\times """+latex(1-pB2).replace('.',',')+r"""$}
            }
        edge from parent [parent anchor=base,->,""",end="")
if not c&0b001:
    print("black",end="")
else:
    print("red",end="")
print(r"""]      
		node[below] {\small $\times """+latex(1-pA).replace('.',',')+r"""$}
    };
\end{tikzpicture}""")
print(r"\end{center}")

print(r"$"+lstQ[q]+"=",end="")
if q==0:
    print(r"p(A \cap B)+p(\overline A \cap B) = ",end="")
    print(latex(pA).replace('.',',')+r" \times "+latex(pB1).replace('.',','),end=" + ")
    print(latex(1-pA).replace('.',',')+r" \times "+latex(pB2).replace('.',','),end=" = ")
    print(latex(pA*pB1).replace('.',','),end=" + ")
    print(latex((1-pA)*pB2).replace('.',','),end=" = ")
    print(latex(pA*pB1+(1-pA)*pB2).replace('.',','),end="")
elif q==1:
    print(r"p(A \cap \overline B)+p(\overline A \cap \overline B) = ",end="")
    print(latex(pA).replace('.',',')+r" \times "+latex(1-pB1).replace('.',','),end=" + ")
    print(latex(1-pA).replace('.',',')+r" \times "+latex(1-pB2).replace('.',','),end=" = ")
    print(latex(pA*(1-pB1)).replace('.',','),end=" + ")
    print(latex((1-pA)*(1-pB2)).replace('.',','),end=" = ")
    print(latex(pA*(1-pB1)+(1-pA)*(1-pB2)).replace('.',','),end="")
elif q==2:
    print(latex(pA).replace('.',',')+r" \times "+latex(pB1).replace('.',','),end=" = ")
    print(latex(pA*pB1).replace('.',','),end="")
elif q==3:
    print(latex(pA).replace('.',',')+r" \times "+latex(1-pB1).replace('.',','),end=" = ")
    print(latex(pA*(1-pB1)).replace('.',','),end="")
elif q==4:
    print(latex(1-pA).replace('.',',')+r" \times "+latex(pB2).replace('.',','),end=" = ")
    print(latex((1-pA)*pB2).replace('.',','),end="")
elif q==5:
    print(latex(1-pA).replace('.',',')+r" \times "+latex(1-pB2).replace('.',','),end=" = ")
    print(latex((1-pA)*(1-pB2)).replace('.',','),end="")
    
print("$")
finRflash()
