if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#-----
from random import *

def écrit(x):
    if x>=0 :
        return str(x)
    else :
        return "("+str(x)+")"

def plusmoins(x):
    if x>0:
        return "+"+str(x)
    else:
        return str(x)
        
#---- premier algorithme
def algo1():
    a0 = randint(1,15)*choice([-1,1])
    b0 = a0+randint(1,15)*choice([-1,1])
    
    code = r"""\begin{codePython}
a,b = """+str(a0)+","+str(b0)+r"""
a = b-a
b = b-a
print("a =",a,", b =",b)
\end{codePython}
"""
    
    debutQflash("Python, affectations")
    print(r"Qu'affiche l'exécution du code suivant ?\\")
    print(code)
    finQflash()
    
    debutRflash()
    print(r"""\begin{tblr}{hlines={3-5}{solid},vline{3-Z}={1-Z}{solid},
	colspec={llVVV},row{1}={gray8,font=\bfseries},
	column1={fg=gray8, font={\texttt \scriptsize}},
	column2={cmd=\piton},
	row{2-Z}={valign=b}}
 & code & a & b \\""")
    
    print(r"1 & a,b = "+str(a0)+","+str(b0)+"& $"+str(a0)+"$ & $"+str(b0)+r"$ \\")
    a,b = a0,b0
    print(r"2 & a = b-a & \SetCell{bg=yellow!50} {{\color{gray}\scriptsize $",b,"-"+écrit(a),r"$}\\$",b-a,r"$} & $",b,r"$\\")
    a = b-a
    print(r"2 & b = b-a & $",a,r"$ & \SetCell{bg=yellow!50} {{\color{gray}\scriptsize $",b,"-"+écrit(a),r"$}\\$",b-a,r"$} \\")
    b = b-a
    print(r"4 & print(...) & \SetCell[c=2]{l,bg=orange!20}affiche a = $",a,"$ , b = $",b,r"$\\")
    print(r"\end{tblr}")

    finRflash()

#---- deuxième algorithme
def algo2():
    a0 = randint(1,15)*choice([-1,1])
    b0 = a0+randint(1,15)*choice([-1,1])
    
    code = r"""\begin{codePython}
a,b = """+str(a0)+","+str(b0)+r"""
a = b-a
b = b-a
a = a+b
print("a =",a,", b =",b)
\end{codePython}
"""
    
    debutQflash("Python, affectations")
    print(r"Qu'affiche l'exécution du code suivant\\")
    print(code)
    finQflash()
    
    debutRflash()
    print(r"""\begin{tblr}{hlines={3-5}{solid},vline{3-Z}={1-Z}{solid},
	colspec={llVV},row{1}={gray8,font=\bfseries},
	column1={fg=gray8, font={\texttt \scriptsize}},
	column2={cmd=\piton},
	row{2-Z}={valign=b}}
 & code & a & b \\""")
    
    print(r"1 & a,b = "+str(a0)+","+str(b0)+"& $"+str(a0)+"$ & $"+str(b0)+r"$ \\")
    a,b = a0,b0
    print(r"2 & a = b-a & \SetCell{bg=yellow!50} {{\color{gray}\scriptsize $",b,"-"+écrit(a),r"$}\\$",b-a,r"$} & $",b,r"$\\")
    a = b-a
    print(r"3 & b = b-a & $",a,r"$ & \SetCell{bg=yellow!50} {{\color{gray}\scriptsize $",b,"-"+écrit(a),r"$}\\$",b-a,r"$} \\")
    b = b-a
    print(r"4 & a = a+b & \SetCell{bg=yellow!50} {{\color{gray}\scriptsize $",a,"+"+écrit(b),r"$}\\$",a+b,r"$} & $",b,r"$\\")
    a = a+b
    print(r"5 & print(...) & \SetCell[c=2]{l,bg=orange!20}affiche a =",a,", b =",b,r"\\")
    print(r"\end{tblr}")

    finRflash()

#---- troisième algorithme
def algo3():
    x0 = randint(1,7)*choice([-1,1])
    px = randint(1,5)*choice([-1,1])
    ma = randint(2,5)*choice([-1,1])
    pa = randint(2,5)*choice([-1,1])
    
    code = r"""\begin{codePython}
def f(x):
    x = x"""+plusmoins(px)+"""
    a = """+str(ma)+"""*x"""+plusmoins(pa)+"""
    return a
\end{codePython}
"""
    
    debutQflash("Python, affectations")
    print(r"On exécute de code suivant ?\\")
    print(code)
    print(r"Que renvoie \piton{f("+str(x0)+r")} ?")
    finQflash()
    
    debutRflash()
    print(r"""\begin{tblr}{hlines={3-5}{solid},vline{3-Z}={1-Z}{solid},
    colspec={llVVV},row{1}={gray8,font=\bfseries},
    column1={fg=gray8, font={\texttt \scriptsize}},
    column2={cmd=\piton},
    row{2-Z}={valign=b}}
 & code & x & a \\""")
    print(r"1 & f("+str(x0)+r") & \SetCell{bg=yellow!50} $"+str(x0)+r"$ & \\")
    x = x0
    print(r"2 & x = x"+plusmoins(px)+r" & \SetCell{bg=yellow!50} {{\color{gray}\scriptsize $",x,plusmoins(px),r"$}\\$",x+px,r"$} & \\")
    x = x+px
    print(r"3 & a = "+str(ma)+"*x"+plusmoins(pa)+r" & $",x,r"$ & \SetCell{bg=yellow!50} {{\color{gray}\scriptsize $",ma,r"\times "+écrit(x),plusmoins(pa),r"$}\\$",ma*x+pa,r"$} \\")
    a = ma*x+pa
    print(r"4 & return a & \SetCell[c=2]{l,bg=orange!20}renvoie $",a,r"$\\")
    print(r"\end{tblr}\\")
    print(r"\medskip")
    print(r"""\begin{tcolorbox}[title={Console Python},colback=white, boxsep=1pt, fontupper=\ttfamily, fonttitle=\bfseries]
\bgroup\small $>>>$\egroup\ \piton{f("""+str(x0)+""")}\\

\py{"""+str(a)+"""}
\end{tcolorbox}""")
    finRflash()

#----
choice([algo1,algo2,algo3])()
