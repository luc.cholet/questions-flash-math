#----
#
# \usepackage[tikz]
# \usetikzlibrary{positioning}
#
#----
if __name__ == "__main__":
    settings = None

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ---- fiche question
from random import choice, randint, random

n = randint(12,100)

if settings!=None and 'n' in settings:
    n = settings['n']
    
d = 2
lst_a = ["1"]
lst_b = [str(n)]

while d*d<n:
    if n%d==0:
        lst_a.append(str(d))
        lst_b.append(str(n//d))
    d = d+1
r = d # dernier diviseur possible si r*r==n

debutQflash("Diviseurs")
print(r"Donner la liste des diviseurs de $"+str(n)+"$")
finQflash()

# ---- fiche réponse
debutRflash()
print(r"Les diviseurs de $"+str(n)+r"$ sont \\")
print(r"\medskip")
k = len(lst_a)

print(r"\begin{tikzpicture}[node distance="+str(min(.5,2/k))+"cm]")
print(r"\node (a0) {$1$};")
last = "a0"
for i in range(1,k):
    print(r"\node (a"+str(i)+r") [right=of "+last+"] {$"+lst_a[i]+"$};")
    last = "a"+str(i)
if r*r==n :
    print(r"\node [draw,shape=circle,draw=orange, inner sep=1pt] (r) [right=of "+last+"] {$"+str(r)+"$};")
    last = "r"
for i in range(k):
    print(r"\node (b"+str(k-1-i)+r") [right=of "+last+"] {$"+lst_b[k-1-i]+"$};")
    last = "b"+str(k-1-i)
for i in range(k):
    print("\draw [<->,orange] (a"+str(i)+") |- +(0,-"+str(.375+(k-i)*.125)+") -| (b"+str(i)+");")
print(r"\end{tikzpicture}\\")
print(r"\medskip")
if r*r!=n  :
    print("($"+str(r-1)+"< \sqrt{"+str(n)+"} <"+str(r)+r"$)\\")
if k==1 and r*r!=n :
    print(r"Remarque : $"+str(n)+"$ est un nombre premier.")
finRflash()
