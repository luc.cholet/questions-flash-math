#----
#
# \usepackage[tabularray]
#
#----
if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ---- fiche question
from random import sample, randint

if settings!=None and 'n' in settings:
    n = settings['n']
else :
    lst = sample([(2,4),(3,3),(5,3),(7,2),(11,2)],4)
    n = 1
    for i in range(randint(2,3)):
        p,e = lst[i]
        n *= p**randint(1,e)

debutQflash("Décomposition")
print(r"Donner décomposition en produit de facteurs premiers de $"+str(n)+"$")
finQflash()

# ---- fiche réponse
debutRflash()
print(r"\begin{minipage}[c]{.3\linewidth}")
q = n
d = 2
lst = []
print(r"\begin{tblr}{vline{2}={1pt,solid},colspec={rl}}")
while q!=1 :
    if q%d==0 :
        print(q,"&",d,r"\\")
        q = q//d
        lst.append(d)
    else:
        d += 1
print(q,r"& \\")
print(r"\end{tblr}")
print(r"\end{minipage}")
print(r"\begin{minipage}[c]{.6\linewidth}")
print(r"On en déduit")
print(r"\["+str(n)+"=",end='')
k = len(lst)
i = 0
while i<k :
    print(lst[i],end='')
    if i+1<k:
        if lst[i]==lst[i+1]:
            e = 1
            print("^{",end='')
            boucle = True
            while i+1<k and boucle :
                if lst[i]==lst[i+1]:
                    e += 1
                    i += 1
                else :
                    boucle = False
            print(str(e)+"}",end='')
        if i+1<k :
            print(r"\times ",end='')
    i += 1
print(r"\]")
print(r"\end{minipage}")
finRflash()
