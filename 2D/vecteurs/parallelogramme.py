if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
#------------------------
from random import *
import math
from sympy import *

def coord(x,y):
    return str((x+y*.5,y*math.sqrt(3)/2))
        
def coef(k,signe=False):
    if k==1:
        if signe :
            return "+"
        else:
            return ""
    elif k==-1 :
        return "-"
    elif k>0 :
        if signe :
            return "+"+latex(k)+r"\,"
        else :
            return latex(k)+r"\,"
    else :
        return latex(k)+r"\,"

directions = sample([(1,0),(0,1),(1,-1)],2)
# choix du vecteur u
dx,dy = directions[0]
lu = randint(1,3)
k1 = choice([-1,1])
l = lu*choice([-1,1])

dx *= l
dy *= l

ux1, ux2 = 0, dx
uy1, uy2 = 0, dy

# choix du vecteur v
dx,dy = directions[1]
lst = list(range(1,4))
lst.remove(abs(l))
lv = choice(lst)
k2 = choice([-1,1])
l = lv*choice([-1,1])
             
dx *= l
dy *= l

vx1, vx2 = 0, dx
vy1, vy2 = 0, dy

#le parallelogramme ABCD
xA,yA = 0,0
xB = xA + ux2
yB = yA + uy2
xC = xB + vx2
yC = yB + vy2

dx = 4-xC//2
dy = 2-yC//2

xA += dx
xB += dx
xC += dx
yA += dy
yB += dy
yC += dy

xD = xA + vx2
yD = yA + vy2

ux1 = xA - vx2/(lv*2)
uy1 = yA - vy2/(lv*2)

vx1 = xA - ux2/(lu*2)
vy1 = yA - uy2/(lu*2)


ux2 += ux1
uy2 += uy1

vx2 += vx1
vy2 += vy1

#choix des trois points pour la combinaison linéraire
x1,y1 = 0,0
x2 = x1 + k1*(ux2-ux1)
y2 = y1 + k1*(uy2-uy1)
x3 = x2 + k2*(vx2-vx1)
y3 = y2 + k2*(vy2-vy1)

dx = 4-x3//2
dy = 2-y3//2

x1 += dx
x2 += dx
x3 += dx
y1 += dy
y2 += dy
y3 += dy
x4 = x1 + x3 - x2
y4 = y1 + y3 - y2

#---- fiche question
debutQflash("Somme de vecteurs")
print(r"Soit un parallélogramme $ABCD$ tel que $\vv{AB}=\vec u$ et $\vv{AD}=\vec v$,\\ on veut construire")
print(r"$"+coef(k1)+r"\vec u"+coef(k2,True)+r"\vec v $")

print(r"""\begin{center}
\begin{tikzpicture}""")
print(r"\draw [thick] "+coord(xA,yA)+r" -- "+coord(xB,yB)+r" -- "+coord(xC,yC)+r" -- "+coord(xD,yD)+r" -- cycle;")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(ux1,uy1)+r" -- node [above=-2pt,sloped] {$\vec u$} "+coord(ux2,uy2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(vx1,vy1)+r" -- node [above=-2pt,sloped] {$\vec v$} "+coord(vx2,vy2)+";")
print(r"\fill [opacity=.5] "+coord(xA,yA)+r" circle [radius=2pt] node [above right, opacity=1] {$A$};")
print(r"\fill [opacity=.5] "+coord(xB,yB)+r" circle [radius=2pt] node [above right, opacity=1] {$B$};")
print(r"\fill [opacity=.5] "+coord(xC,yC)+r" circle [radius=2pt] node [above right, opacity=1] {$C$};")
print(r"\fill [opacity=.5] "+coord(xD,yD)+r" circle [radius=2pt] node [above right, opacity=1] {$D$};")
print(r"""\end{tikzpicture}
\end{center}""")
finQflash()

#---- fiche réponse
debutRflash()
r = r"\["+coef(k1)+r"\vec u"+coef(k2,True)+r"\vec v = \vv{"
if k1>0 :
    if k2>0 :
        r += "AC"
    else:
        r += "DB"
else:
    if k2>0 :
        r += "BD"
    else:
        r += "CA"
r += r"}\]"
print(r)
print(r"""\begin{center}
\begin{tikzpicture}""")
print(r"\draw [thick] "+coord(xA,yA)+r" -- "+coord(xB,yB)+r" -- "+coord(xC,yC)+r" -- "+coord(xD,yD)+r" -- cycle;")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(ux1,uy1)+r" -- node [above=-2pt,sloped] {\small $\vec u$} "+coord(ux2,uy2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(vx1,vy1)+r" -- node [above=-2pt,sloped] {\small $\vec v$} "+coord(vx2,vy2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(x1,y1)+r" -- node [above=-2pt,sloped] {\small $"+coef(k1)+r"\vec u$} "+coord(x2,y2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(x2,y2)+r" -- node [above=-2pt,sloped] {\small $"+coef(k2)+r"\vec v$} "+coord(x3,y3)+";")
print(r"\draw [red,line width=2pt,-latex] "+coord(x1,y1)+r" -- node [above=-2pt,sloped] {\small $"+coef(k1)+r"\vec u"+coef(k2,True)+r"\vec v$} "+coord(x3,y3)+";")
print(r"\fill [opacity=.5] "+coord(xA,yA)+r" circle [radius=2pt] node [above right, opacity=1] {$A$};")
print(r"\fill [opacity=.5] "+coord(xB,yB)+r" circle [radius=2pt] node [above right, opacity=1] {$B$};")
print(r"\fill [opacity=.5] "+coord(xC,yC)+r" circle [radius=2pt] node [above right, opacity=1] {$C$};")
print(r"\fill [opacity=.5] "+coord(xD,yD)+r" circle [radius=2pt] node [above right, opacity=1] {$D$};")
print(r"""\end{tikzpicture}
\end{center}""")
finRflash()	