if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
#------------------------
from random import *

# point de départ P1 et P5
m = choice([0b1,0b10000])
# point d'arrivé P4 et P6
m += choice([0b1000,0b100000])
# point intermédiaire P2,P3
m += choice([0b10,0b100])

points = sample("ABCDEFGHIJLMNOPQRSTUV",3)

#---- fiche question
debutQflash(r"Relation de \bsc{Chasles}")
print(r"Compléter à l'aide de la relation de \bsc{Chasles}")
Chasles = r"\[ \vv{P1P2}+\vv{P3P4}=\vv{P5P6}\]"

if m&1:
    Chasles = Chasles.replace("P1",points[0])
else :
    Chasles = Chasles.replace("P1",r"\bgroup \color{red} ~?~ \egroup{}")
if m&2:
    Chasles = Chasles.replace("P2",points[1])
else :
    Chasles = Chasles.replace("P2",r"\bgroup \color{red} ~?~ \egroup{}")
if m&4:
    Chasles = Chasles.replace("P3",points[1])
else :
    Chasles = Chasles.replace("P3",r"\bgroup \color{red} ~?~ \egroup{}")
if m&8:
    Chasles = Chasles.replace("P4",points[2])
else :
    Chasles = Chasles.replace("P4",r"\bgroup \color{red} ~?~ \egroup{}")
if m&16:
    Chasles = Chasles.replace("P5",points[0])
else :
    Chasles = Chasles.replace("P5",r"\bgroup \color{red} ~?~ \egroup{}")
if m&32:
    Chasles = Chasles.replace("P6",points[2])
else :
    Chasles = Chasles.replace("P6",r"\bgroup \color{red} ~?~ \egroup{}")
print(Chasles)
finQflash()

#---- fiche réponse
debutRflash()
print(r"D'après de la relation de \bsc{Chasles}")
print(r"\settowidth\largeur{$"+points[1]+"+"+points[1]+"$}")

Chasles = r"\[ \vv{P1 \tikz [overlay] \fill[blue!20, rounded corners=4pt] (0,-1pt) rectangle (\largeur+1pt,1.5ex);P2}+\vv{P3P4}=\vv{P5P6}\]"
if m&1:
    Chasles = Chasles.replace("P1",points[0])
else :
    Chasles = Chasles.replace("P1",r"\bgroup \color{red}"+points[0]+r"\egroup{}")
if m&2:
    Chasles = Chasles.replace("P2",points[1])
else :
    Chasles = Chasles.replace("P2",r"\bgroup \color{red}"+points[1]+r"\egroup{}")
if m&4:
    Chasles = Chasles.replace("P3",points[1])
else :
    Chasles = Chasles.replace("P3",r"\bgroup \color{red}"+points[1]+r"\egroup{}")
if m&8:
    Chasles = Chasles.replace("P4",points[2])
else :
    Chasles = Chasles.replace("P4",r"\bgroup \color{red}"+points[2]+r"\egroup{}")
if m&16:
    Chasles = Chasles.replace("P5",points[0])
else :
    Chasles = Chasles.replace("P5",r"\bgroup \color{red}"+points[0]+r"\egroup{}")
if m&32:
    Chasles = Chasles.replace("P6",points[2])
else :
    Chasles = Chasles.replace("P6",r"\bgroup \color{red}"+points[2]+r"\egroup{}")
print(Chasles)
finRflash()