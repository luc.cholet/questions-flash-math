if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#------------------------
from random import *
import math
from sympy import *

def coef(k,signe=False):
    if k==1:
        if signe :
            return "+"
        else:
            return ""
    elif k==-1 :
        return "-"
    elif k>0 :
        if signe :
            return "+"+latex(k)+r"\,"
        else :
            return latex(k)+r"\,"
    else :
        return latex(k)+r"\,"

lst = sample(range(0,7),2)
x1 = lst[0]
x2 = lst[1]

lst = sample(range(5,12),2)
x3 = lst[0]
x4 = lst[1]

#---- fiche question
debutQflash("Coefficient entre vecteurs")
print("Sur la droite ci-dessous, les points sont régulièrement espacés.")

print(r"""\begin{tikzpicture}
\draw [thick] (-0.5,0) -- (11.5,0);""")
for i in range(12):
    print(r"\fill [black, opacity=.5] (",i,",0.0) circle [radius=2pt] node [below, opacity=1] {$"+chr(65+i)+"$};")
print(r"\draw [green!50!black, line width=2pt, arrows ={-Latex[scale=1.1]},opacity=.75] (",x1,r",0.05) -- node [above,opacity=1] {$\vv{"+chr(65+x1)+chr(65+x2)+"}$} +(",x2-x1,",0);")

print(r"\draw [red, line width=2pt, arrows ={-Latex[scale=1.1]}, opacity=.75] (",x3,r",-0.05) -- node [below=1.4ex,opacity=1] {$\vv{"+chr(65+x3)+chr(65+x4)+"}$} +(",x4-x3,",0);")
print(r"\end{tikzpicture}\\")
print(r"\medskip")
print(r"Exprimer le vecteur $\vv{"+chr(65+x3)+chr(65+x4)+r"}$ en fonction du vecteur $\vv{"+chr(65+x1)+chr(65+x2)+"}$.")
finQflash()

#---- fiche réponse
debutRflash()
print(r"""\begin{tikzpicture}
\draw [thick] (-0.5,0) -- (11.5,0);""")
for i in range(12):
    print(r"\fill [black, opacity=.5] (",i,",0.0) circle [radius=2pt] node [below, opacity=1] {$"+chr(65+i)+"$};")
print(r"\draw [green!50!black, line width=2pt, arrows ={-Latex[scale=1.1]},opacity=.75] (",x1,r",0.05) -- node [above,opacity=1] {$\vv{"+chr(65+x1)+chr(65+x2)+"}$} +(",x2-x1,",0);")

print(r"\draw [red, line width=2pt, arrows ={-Latex[scale=1.1]}, opacity=.75] (",x3,r",-0.05) -- node [below=1.4ex,opacity=1] {$\vv{"+chr(65+x3)+chr(65+x4)+"}$} +(",x4-x3,",0);")
print(r"\end{tikzpicture}\\")
print(r"\medskip")
print(r"\[ \vv{"+chr(65+x3)+chr(65+x4)+r"} ="+coef(Rational(x4-x3,x2-x1))+r"\vv{"+chr(65+x1)+chr(65+x2)+"}\]")
finRflash()