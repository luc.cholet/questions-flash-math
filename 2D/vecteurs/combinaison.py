if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
#------------------------
from random import *
import math
from sympy import *

def coord(x,y):
    return str((x+y*.5,y*math.sqrt(3)/2))
        
def coef(k,signe=False):
    if k==1:
        if signe :
            return "+"
        else:
            return ""
    elif k==-1 :
        return "-"
    elif k>0 :
        if signe :
            return "+"+latex(k)+r"\,"
        else :
            return latex(k)+r"\,"
    else :
        return latex(k)+r"\,"

directions = sample([(1,0),(0,1),(1,-1)],2)
# choix du vecteur u
dx,dy = directions[0]
l = randint(1,3)
k1 = choice([-1,1])*sympify(choice( [ [0], [1, '3/2', 2, '5/2'], ['1/2', 1, '3/2', '2'], ['1/3', '2/3', 1, '4/3'] ][l] ))
l *= choice([-1,1])

dx *= l
dy *= l

ux1, ux2 = 0, dx
uy1, uy2 = 0, dy

dy = 6-max(uy1,uy2)
uy1 += dy
uy2 += dy

dx = min(uy1//2+ux1, uy2//2+ux2)
ux1 -= dx
ux2 -= dx

# choix du vecteur v
dx,dy = directions[1]
lst = list(range(1,4))
lst.remove(abs(l))
l = choice(lst)
k2 = choice([-1,1])*sympify(choice( [ [0], [1, '3/2', 2, '5/2'], ['1/2', 1, '3/2', '2'], ['1/3', '2/3', 1, '4/3'] ][l] ))
l *= choice([-1,1])

dx *= l
dy *= l

vx1, vx2 = 0, dx
vy1, vy2 = 0, dy

dy = min(vy1,vy2)
vy1 -= dy
vy2 -= dy

dx = min(vy1//2+vx1, vy2//2+vx2)
vx1 -= dx
vx2 -= dx

#choix des trois points pour la combinaison linéraire
x1,y1 = 0,0
x2 = x1 + k1*(ux2-ux1)
y2 = y1 + k1*(uy2-uy1)
x3 = x2 + k2*(vx2-vx1)
y3 = y2 + k2*(vy2-vy1)

dx = 4-x3//2
dy = 3-y3//2

x1 += dx
x2 += dx
x3 += dx
y1 += dy
y2 += dy
y3 += dy

#---- fiche question
debutQflash("Somme de vecteurs")
print("Dans le pavage ci-dessous, on veut construire")
print(r"\["+coef(k1)+r"\vec u"+coef(k2,True)+r"\vec v \]")

print(r"""\vspace{1cm}\begin{tikzpicture}
\draw[very thick] (-0.25,-.38) rectangle +(10,6);
\clip (-.25,-.38) rectangle +(10,6);
\foreach \x in {-6,...,13} {
   \begin{scope}[thin,gray!44,densely dashed]
	\draw (\x+.5,0) ++(60:-1) -- +(60:10);
	\draw (120:\x-.5) ++(-1,0) -- +(11+\x,0);
	\draw (\x-.5,0) ++(60:-1) -- +(120:10);
   \end{scope}
   \begin{scope}[thin,gray!77]
	\draw (\x,0) ++(60:-1) -- +(60:10);
	\draw (120:\x) ++(-1,0) -- +(11+\x,0);
	\draw (\x,0) ++(60:-1) -- +(120:10);
   \end{scope}
}""")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(ux1,uy1)+r" -- node [above=-2pt,sloped] {$\vec u$} "+coord(ux2,uy2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(vx1,vy1)+r" -- node [above=-2pt,sloped] {$\vec v$} "+coord(vx2,vy2)+";")
print(r"""\draw[very thick] (-0.25,-.38) rectangle +(10,6);
\end{tikzpicture}""")
finQflash()

#---- fiche réponse
debutRflash()
print(r"\["+coef(k1)+r"\vec u"+coef(k2,True)+r"\vec v \]")

print(r"""\vspace{1cm}\begin{tikzpicture}
\draw[very thick] (-0.25,-.38) rectangle +(10,6);
\clip (-.25,-.38) rectangle +(10,6);
\foreach \x in {-6,...,13} {
   \begin{scope}[thin,gray!44,densely dashed]
	\draw (\x+.5,0) ++(60:-1) -- +(60:10);
	\draw (120:\x-.5) ++(-1,0) -- +(11+\x,0);
	\draw (\x-.5,0) ++(60:-1) -- +(120:10);
   \end{scope}
   \begin{scope}[thin,gray!77]
	\draw (\x,0) ++(60:-1) -- +(60:10);
	\draw (120:\x) ++(-1,0) -- +(11+\x,0);
	\draw (\x,0) ++(60:-1) -- +(120:10);
   \end{scope}
}""")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(ux1,uy1)+r" -- node [above=-2pt,sloped] {$\vec u$} "+coord(ux2,uy2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(vx1,vy1)+r" -- node [above=-2pt,sloped] {$\vec v$} "+coord(vx2,vy2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(x1,y1)+r" -- node [above=-2pt,sloped] {\tiny $"+coef(k1)+r"\vec u$} "+coord(x2,y2)+";")
print(r"\draw [green!50!black,line width=2pt,-latex] "+coord(x2,y2)+r" -- node [above=-2pt,sloped] {\tiny $"+coef(k2)+r"\vec v$} "+coord(x3,y3)+";")
print(r"\draw [red,line width=2pt,-latex] "+coord(x1,y1)+r" -- node [above=-2pt,sloped] {\tiny $"+coef(k1)+r"\vec u"+coef(k2,True)+r"\vec v$} "+coord(x3,y3)+";")
print(r"\fill [opacity=.5] "+coord(x1,y1)+r" circle [radius=2pt];")
print(r"\fill [opacity=.5] "+coord(x2,y2)+r" circle [radius=2pt];")
print(r"\fill [opacity=.5] "+coord(x3,y3)+r" circle [radius=2pt];")
print(r"""\draw[very thick] (-0.25,-.38) rectangle +(10,6);
\end{tikzpicture}""")
finRflash()	