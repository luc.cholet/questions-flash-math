if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ---- fiche question
from random import choice, randint, random
from sympy import symbols, latex

x = symbols('x')

a = choice([-1, 1])
if random()<.8 :
    a = 0
lst = list(range(-5, 6))
lst.remove(0)
b = choice(lst)
c = choice(lst)

e = a*x**2+b*x+c
xA = randint(-6, 6)
yA = e.subs(x, xA)

debutQflash("Calcul d'image")
print(r"Soit la fonction $f$ définie par")
print(r"\[ f \colon x \mapsto "+latex(e).replace('x', r'\,x')+r"\]")
print(r"Calculer l'image de $"+str(xA)+r"$ par $f$")
finQflash()

# ---- fiche réponse
debutRflash()
print(r"\[ f \colon x \mapsto "+latex(e).replace('x', r'\,x')+r"\]")

s = latex(e)

if xA >= 0:
    sxA = str(xA)
else:
    sxA = "("+str(xA)+")"

if a != 0:
    if not (a in [-1, 1]):
        s = s.replace("x", r"\times "+sxA, 1)
    else:
        s = s.replace("x", sxA, 1)

if b != 0:
    if not (b in [-1, 1]):
        s = s.replace("x", r"\times "+sxA, 1)
    else:
        s = s.replace("x", sxA, 1)


print(r"$f("+str(xA)+")="+s+r"="+latex(yA)+r"$\\")
print(r"\medskip")
print("donc l'image de $"+str(xA)+"$ vaut $"+latex(yA)+r"$\\")
finRflash()
