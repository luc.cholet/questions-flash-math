if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
        
#----------
from random import *
import math

def draw(x,y):
    n = len(x)
    
    # définie les pentes pour chaque point...
    a = [0]*n
    l = [0]*(n-1)
    
    for i in range(n-1) :
        l[i] = (x[i+1]-x[i])/3

    for i in range(1,n-1) :
        if (y[i-1]-y[i])*(y[i+1]-y[i]) >= 0 :
            a[i] = 0
        else :
            a[i] =  math.tan( ( math.atan((y[i-1]-y[i])/(x[i-1]-x[i]))+math.atan((y[i+1]-y[i])/(x[i+1]-x[i])) )/2 )
            
    a[0] = (y[1]-y[0]-2*l[0]*a[1])/l[0]
    
    if y[1]>=y[0] :
        a[0] = max(0,a[0])
    else :
        a[0] = min(0,a[0])
    
    a[-1] = (y[-1]-y[-2]-2*l[-1]*a[-2])/l[-1]

    if y[-2]<=y[-1] :
        a[-1] = max(0,a[-1])
    else :
        a[-1] = min(0,a[-1])
    
    output = r"\draw [green!50!black, line width=2pt] ("+str(x[0])+r","+str(y[0])+r") .. controls ("+str(x[0]+l[0])+r","+str(y[0]+a[0]*l[0])+r") and "
    for i in range(1,n-1) :
        output += "("+str(x[i]-l[i-1])+","+str(y[i]-l[i-1]*a[i])
        output += ") .. ("+str(x[i])+","+str(y[i])
        output += ") .. controls ("+str(x[i]+l[i])+","+str(y[i]+l[i]*a[i])+") and "
    output += "("+str(x[-1]-l[-1])+r","+str(y[-1]-l[-1]*a[-1])+r") .. ("+str(x[-1])+r","+str(y[-1])+");\n"
    
    print(output)


choix = [".+0-.",".-0+.",".+0+.",".-0-.",".+0-0+.",".-0+0-.",".-0-0+.",".+0-0+."]
c = choice(choix)
val = randint(-2,2)
ymin = val-3
ymax = val+3
n = len(c)
x = [randint(-4,2)]
y = []

for i in range(n):
    if c[i] in "0." :
        y.append(val)
    elif c[i] == "+" :
        y.append(randint(2*val+1,2*ymax)/2)
    elif c[i] == "-" :
        y.append(randint(2*ymin,2*val-1)/2)
    i += 1
    if i!= n :
        x.append(x[-1]+randint(1,2))
if c[0] == "." :
    y[0] = y[1]
    y[choice([0,1])] = .2*val+.8*y[1]
if c[-1] == "." :
    y[-1] = y[-2]
    y[choice([-1,-2])] = .2*val+.8*y[-2]

xmin = min(-1,x[0]-1)
xmax = max(8,x[-1]+1)

#---- fiche question
debutQflash("Lecture graphique d'inéquation")
comp = ["<" , r"\leq", ">", r"\geq"]
inegalite = randrange(4)
t = [(lambda k : y[k]<val), (lambda k : y[k]<=val), (lambda k : y[k]>val), (lambda k : y[k]>=val)][inegalite]

    
print(r"Lire graphique les solutions de l'inéquation $f(x)"+comp[inegalite]+str(val)+r"$ où la fonction $f$ est définie par la représentation graphique ci-dessous.")
print()
print(r"\begin{tikzpicture}[scale=.7]")
print(r"\tkzInit[xmin=",xmin,r",xmax=",xmax,r",xstep=1,ymin=",ymin,r",ymax=",ymax,r",ystep=1]",sep="")
print(r"\tkzGrid[fill=none]")
print(r"\tkzAxeXY[fill=none]")

draw(x,y)

for i in [0,n-1] :
    #print(r"\draw [orange] (",x[i]-.5,r",",y[i]-.5*a[i],r") --  (",x[i]+.5,r",",y[i]+.5*a[i],r");")
    print(r"\fill [green!50!black] (",x[i],r",",y[i],r") circle [radius=3pt];")
    
print(r"\end{tikzpicture}")

finQflash()

#---- fiche réponse
debutRflash()
sol = r"$f(x)"+comp[inegalite]+str(val)+r"$ ssi $x\in"
racines = []
for j in range(2,n-2,2) :
    if t(j) and inegalite&1 == 1: # que pour les inégalités larges
        racines.append(x[j])
intervalles = []
for j in range(1,n-1,2) :
    if t(j) :
        intervalles.append( x[j-1] )
        intervalles.append( x[j+1] )
#---- optimisation des intervalles
N = len(intervalles)
k = 1 # on commence à la première borne sup
while N>2 and k<N-2 : # il faut au moins deux intervalles donc 3 bornes d'intervalles
    if intervalles[k] == intervalles[k+1] :
        if intervalles[k] in racines :
            r = intervalles[k]
            N -= 2
            intervalles.remove(r)
            intervalles.remove(r)
            racines.remove(r)
        else :
            k += 2
    else :
        k += 2
        
#---- affiche les intervalles en premier
first = True
N = len(intervalles)
k = 0 # on commence à la première borne inf
while k<N :
    if not first :
        sol += r"\cup"
    first = False
    ai = intervalles[k]
    b = intervalles[k+1]
    if ai in racines or (ai == x[0] and t(0)):
        sol = sol+r"\mathopen{[}"
        if ai in racines :
            racines.remove(ai)
    else :
        sol += r"\mathopen{]}"
    sol += r"\,"+str(ai)+r"\,;\,"+str(b)+r"\,"
    if b in racines or (b == x[-1] and t(-1)) :
        sol += r"\mathclose{]}"
        if b in racines :
            racines.remove(b)
    else :
        sol += r"\mathclose{[}"
    k += 2
        
#---- affiche les solutions restantes
if len(racines) != 0 or first :
    if not first :
        sol += r"\cup"
    sol += r"\{\,"
    first = True
    for s in racines:
        if not first :
            sol += r";\,"
        first = False
        sol += str(s)+r"\,"
    sol += r"\}"

sol += r"$"

print(r"\begin{tikzpicture}[scale=.7]")
print(r"\tkzInit[xmin=",xmin,r",xmax=",xmax,r",xstep=1,ymin=",ymin,r",ymax=",ymax,r",ystep=1]",sep="")

N = len(intervalles)
k = 0
while k<N :
    print(r"\fill [orange!20] (",intervalles[k],",",ymin,") rectangle (",intervalles[k+1],",",ymax,r");")
    k += 2
    
print(r"\tkzGrid[fill=none]")
print(r"\tkzAxeXY[fill=none]")

draw(x,y)

for i in [0,n-1] :
    print(r"\fill [green!50!black] (",x[i],r",",y[i],r") circle [radius=3pt];")

print(r"\draw [red,very thick] (",xmin,",",val,r") -- (",xmax,",",val,r") node [right] {$y="+str(val)+r"$};")

for i in range(n) :
    if c[i] == "0" :
        print(r"\fill [red] (",x[i],r",",val,r") circle [radius=4pt];")

print(r"\end{tikzpicture}\\")
print()

print(sol)
    
finRflash()
