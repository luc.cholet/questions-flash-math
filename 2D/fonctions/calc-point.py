if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#---- fiche question
from random import *
from sympy import *

x = symbols('x')

a = choice([-1, 1])
if random()<.8 :
    a = 0
lst = list(range(-5,6))
lst.remove(0)
b = choice(lst)
c = choice(lst)

e = a*x**2+b*x+c
xA = randint(-5,5)
yA = e.subs(x,xA)
erreur = choice([-1,0,1,0])

debutQflash(r"\small Appartenance d'un point à une courbe")
print(r"Soit la fonction $f$ définie par")
print(r"\[ f \colon x \mapsto "+latex(e).replace('x','\,x')+r"\]")
print(r"et $\mathcal{C}_f$ sa courbe représentative dans un repère du plan.\\")
print(r"\medskip")
print()
print(r"Dans ce repère, le point $A("+str(xA)+";"+str(yA+erreur)+r")$")
print(r"est-il un point de la courbe $\mathcal{C}_f$ ?\\")
finQflash()

#---- fiche réponse
debutRflash()
print(r"\[ f \colon x \mapsto "+latex(e).replace('x','\,x')+r"\]")

s = latex(e)

if xA>=0 :
    sxA = str(xA)
else :
    sxA = "("+str(xA)+")"

if a != 0 :
    if not (a in [-1,1]) :
        s = s.replace("x",r"\times "+sxA,1)
    else :
        s = s.replace("x",sxA,1)
        
if b != 0 :
    if not (b in [-1,1]) :
        s = s.replace("x",r"\times "+sxA,1)
    else :
        s = s.replace("x",sxA,1)


print(r"$f("+str(xA)+")="+s+r"="+latex(e.subs(x,xA))+r"$\\")
print(r"\medskip")
print()
print(r"$A("+str(xA)+";"+str(yA+erreur)+")$ donc ")
if erreur == 0:
    print(r"$A \in \mathcal{C}_f$")
else :
    print(r"$A \notin \mathcal{C}_f$")
    
finRflash()
