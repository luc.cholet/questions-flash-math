if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")

    def finQflash():
        print("%fin question")

    def debutRflash():
        print("%---- début réponse")

    def finRflash():
        print("%fin question")

# ---- fiche question
from random import choice, randint, random
from sympy import *

x = symbols('x')

a,b = choice( [(2,3), (-2,3), (1,3), (-1,3), (4,3), (-4,3), (-1,1), (1,1), (2,1), (-2,2), (1,2), (-1,2), (3,2), (-3,2)] )
m = Rational(a,b)

p = randint(-1,3)

ymax = max([0,p,2*a+p])+1
ymin = min([0,p,2*a+p])-1
if ymax-ymin>8:
    yscl = 8/(ymax-ymin)
else :
    yscl = 1

debutQflash("droite et expression affine")
print(r"Trouver l'équation réduite de la droite, ou la fonction affine dont la droite est la représentation.")
print(r"\begin{center}")
print(r"\begin{tikzpicture}[xscale=.7,yscale=",.7*yscl,r"]",sep="")
print(r"\tkzInit[xmin=-3,xmax=8,xstep=1,ymin=",ymin,r",ymax=",ymax,r",ystep=1]",sep="")
print(r"\tkzGrid")
print(r"\tkzAxeXY[font=\small,fill=none]")
print(r"\tkzFct[green!50!black,line width=1pt, domain=-6:10]{",a/b,r"*x+",p,r"}",sep="")
print(r"\end{tikzpicture}")
print(r"\end{center}")



finQflash()

# ---- fiche réponse
debutRflash()
print(r"Expression affine de la forme $m\,x+p$ où\\")
print(r"\begin{center}")
print(r"\begin{tikzpicture}[xscale=.7,yscale=",.7*yscl,r"]",sep="")
print(r"\tkzInit[xmin=-3,xmax=8,xstep=1,ymin=",ymin,r",ymax=",ymax,r",ystep=1]",sep="")
print(r"\tkzGrid")
print(r"\tkzAxeXY[font=\small,fill=none]")
print(r"\fill [orange] (0,",p,r") circle [radius=2pt] node [left=2pt,fill=orange!20,rounded corners] {$p=",p,r"$};",sep="")
if m>0:
    print(r"\draw [red,thick] (0,",p,r") -- node [below] {$1$} ++(1,0);",sep="")
else:
    print(r"\draw [red,thick] (0,",p,r") -- node [above] {$1$} ++(1,0);",sep="")
print(r"\draw [red, -latex, very thick] (1,",p,r") -- node [right=2pt,fill=red!20,rounded corners] {$m="+latex(m)+r"$} ++(0,",float(m),r");",sep="")
if b!=1:
    if m<0:
        print(r"\draw [red,thick] (",b,r",",a+p,r") -- node [above] {$",b,r"$} ++(",b,r",0);",sep="")
    else:
        print(r"\draw [red,thick] (",b,r",",a+p,r") -- node [below] {$",b,r"$} ++(",b,r",0);",sep="")
    print(r"\draw [red, -latex, very thick] (",2*b,r",",a+p,r") -- node [right] {$",a,r"$} ++(0,",a,r");",sep="")
print(r"\tkzFct[green!50!black,line width=1pt, domain=-6:10]{",a/b,r"*x+",p,r"}",sep="")
print(r"\end{tikzpicture}")
print(r"\end{center}")
print(r"L'équation réduite de la droite est $y = "+latex(m*x+p).replace('x', r'\,x')+r"$\\")
print(r"La droite est la représentation de la fonction $f \colon x \mapsto "+latex(m*x+p).replace('x', r'\,x')+r"$")
finRflash()
