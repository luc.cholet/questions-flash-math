if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#----------
from random import *
import math

def draw(x,y):
    n = len(x)
    
    # définie les pentes pour chaque point...
    a = [0]*n
    l = [0]*(n-1)
    
    for i in range(n-1) :
        l[i] = (x[i+1]-x[i])/3

    for i in range(1,n-1) :
        if (y[i-1]-y[i])*(y[i+1]-y[i]) >= 0 :
            a[i] = 0
        else :
            a[i] =  math.tan( ( math.atan((y[i-1]-y[i])/(x[i-1]-x[i]))+math.atan((y[i+1]-y[i])/(x[i+1]-x[i])) )/2 )
            
    a[0] = (y[1]-y[0]-2*l[0]*a[1])/l[0]
    
    if y[1]>=y[0] :
        a[0] = max(0,a[0])
    else :
        a[0] = min(0,a[0])
    
    a[-1] = (y[-1]-y[-2]-2*l[-1]*a[-2])/l[-1]

    if y[-2]<=y[-1] :
        a[-1] = max(0,a[-1])
    else :
        a[-1] = min(0,a[-1])
    
    output = r"\draw [green!50!black, line width=2pt] ("+str(x[0])+r","+str(y[0])+r") .. controls ("+str(x[0]+l[0])+r","+str(y[0]+a[0]*l[0])+r") and "
    for i in range(1,n-1) :
        output += "("+str(x[i]-l[i-1])+","+str(y[i]-l[i-1]*a[i])
        output += ") .. ("+str(x[i])+","+str(y[i])
        output += ") .. controls ("+str(x[i]+l[i])+","+str(y[i]+l[i]*a[i])+") and "
    output += "("+str(x[-1]-l[-1])+r","+str(y[-1]-l[-1]*a[-1])+r") .. ("+str(x[-1])+r","+str(y[-1])+");\n"
    
    print(output)
    
choix = [".+0-.",".-0+.",".+0+.",".-0-.",".+0-0+.",".-0+0-.",".-0-0+.",".+0-0+."]
c = choice(choix)
n = len(c)
x = [randint(-4,2)]
y = []

for i in range(n):
    if c[i] in "0." :
        y.append(0)
    elif c[i] == "+" :
        y.append(randint(1,5)/2)
    elif c[i] == "-" :
        y.append(-randint(1,5)/2)
    i += 1
    if i!= n :
        x.append(x[-1]+randint(1,2))
if c[0] == "." :
    y[0] = y[1]*choice([0.8,1.2])
if c[-1] == "." :
    y[-1] = y[-2]*choice([0.8,1.2])

xmin = min(-1,x[0]-1)
xmax = max(8,x[-1]+1)

#---- fiche question
debutQflash("Lecture graphique de signes")

print(r"Établir le tableau de signes de la fonction $f$ dont la représentation graphique est\\")
print()
print(r"\begin{tikzpicture}[scale=.7]")
print(r"\tkzInit[xmin=",xmin,r",xmax=",xmax,r",xstep=1,ymin=-3,ymax=3,ystep=1]")
print(r"\tkzGrid[fill=none]")
print(r"\tkzAxeXY[fill=none]")

draw(x,y)

for i in [0,n-1] :
    #print(r"\draw [orange] (",x[i]-.5,r",",y[i]-.5*a[i],r") --  (",x[i]+.5,r",",y[i]+.5*a[i],r");")
    print(r"\fill [green!50!black] (",x[i],r",",y[i],r") circle [radius=3pt];")

print(r"\end{tikzpicture}")
        
finQflash()

#---- fiche réponse
debutRflash()

print(r"\begin{center}")

print(r"\begin{tikzpicture}[scale=.4]")
print(r"\tkzInit[xmin=",xmin,r",xmax=",xmax,r",xstep=1,ymin=-3,ymax=3,ystep=1]")
print(r"\tkzGrid")
print(r"\tkzAxeXY[font=\tiny,fill=none]")

draw(x,y)

for i in [0,n-1] :
    print(r"\fill [green!50!black] (",x[i],r",",y[i],r") circle [radius=4pt];")
    
for i in range(n) :
    if c[i] == "0" :
        print(r"\fill [red] (",x[i],r",0) circle [radius=4pt];")

print(r"\end{tikzpicture}\\")
print(r"\medskip")
print(r"\begin{tikzpicture}")
print(r"\tkzTabInit [espcl=1.5] {$x$ / .5 ,signe\\de $f(x)$ /1 }{",end="")
for i in range(0,n,2):
    print(r"$",x[i],r"$",end="")
    if i < n-1 :
        print(r",",end="")
    else :
        print(r"}")
print(r"\tkzTabLine{")
for i in range(n) :
    if c[i] == "0" :
        print("z",end="")
    if c[i] == "+" :
        print("+",end="")
    if c[i] == "-" :
        print("-",end="")
    if i != n-1 :
        print(",",end="")
print(r"}")
print(r"\end{tikzpicture}")
print(r"\end{center}")

finRflash()
