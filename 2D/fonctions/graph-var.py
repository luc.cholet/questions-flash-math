if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#----------
from random import *
import math

def draw(x,y):
    n = len(x)
    
    # définie les pentes pour chaque point...
    a = [0]*n
    l = [0]*n
    
    l[0] = (x[1]-x[0])/3
    l[-1] = (x[-1]-x[-2])/3
    
    for i in range(1,n-1) :
        l[i] = min(x[i+1]-x[i],x[i]-x[i-1])/3

    for i in range(1,n-1) :
        if (y[i-1]-y[i])*(y[i+1]-y[i]) >= 0 :
            a[i] = 0
        else :
            a[i] =  math.tan( ( math.atan((y[i-1]-y[i])/(x[i-1]-x[i]))+math.atan((y[i+1]-y[i])/(x[i+1]-x[i])) )/2 )
            
    a[0] = (y[1]-y[0]-2*l[1]*a[1])/(x[1]-2*l[1]-x[0])
    
    if y[1]>=y[0] :
        a[0] = max(0,a[0])
    else :
        a[0] = min(0,a[0])
    
    a[-1] = (y[-1]-y[-2]-2*l[-2]*a[-2])/(x[-1]-x[-2]-2*l[-2])

    if y[-2]<=y[-1] :
        a[-1] = max(0,a[-1])
    else :
        a[-1] = min(0,a[-1])
    
    output = r"\draw [green!50!black, line width=2pt] ("+str(x[0])+r","+str(y[0])+r") .. controls ("+str(x[0]+l[0])+r","+str(y[0]+a[0]*l[0])+r") and "
    for i in range(1,n-1) :
        output += "("+str(x[i]-l[i])+","+str(y[i]-l[i]*a[i])
        output += ") .. ("+str(x[i])+","+str(y[i])
        output += ") .. controls ("+str(x[i]+l[i])+","+str(y[i]+l[i]*a[i])+") and "
    output += "("+str(x[-1]-l[-1])+r","+str(y[-1]-l[-1]*a[-1])+r") .. ("+str(x[-1])+r","+str(y[-1])+");\n"
    
    print(output)

m0 = choice([-1,+1])
m = m0
n = randint(2,3) # de 2 à 3 intervalles de monotonie

x = [randint(-4,2)]
y = [randint(-3,3)]

for i in range(n):
    x.append(x[-1]+randint(3,6-n))
    y.append(y[-1]+randint(1,3)*m)
    m = -m
    
n = len(x)
    
xmin = min(-1,x[0]-1)
xmax = max(8,x[-1]+1)
ymin = min(0,min(y)-1)
ymax = max(max(ymin+6,max(y)+1),0)

#---- fiche question
debutQflash("Lecture graphique de variations")

print(r"Établir le tableau des variations de la fonction $f$ dont la représentation graphique est\\")
print()
print(r"\begin{tikzpicture}[scale=.7]")
print(r"\tkzInit[xmin=",xmin,r",xmax=",xmax,r",xstep=1,ymin=",ymin,r",ymax=",ymax,r",ystep=1]",sep="")
print(r"\tkzGrid[fill=none]")
print(r"\tkzAxeXY[fill=none]")

draw(x,y)

for i in [0,n-1] :
    #print(r"\draw [orange] (",x[i]-.5,r",",y[i]-.5*a[i],r") --  (",x[i]+.5,r",",y[i]+.5*a[i],r");")
    print(r"\fill [green!50!black] (",x[i],r",",y[i],r") circle [radius=3pt];")

print(r"\end{tikzpicture}")
        
finQflash()

#---- fiche réponse
debutRflash()

print(r"\begin{center}")
print(r"\begin{tikzpicture}[scale=.4]")
print(r"\tkzInit[xmin=",xmin,r",xmax=",xmax,r",xstep=1,ymin=",ymin,r",ymax=",ymax,r",ystep=1]",sep="")
print(r"\tkzGrid")
print(r"\tkzAxeXY[font=\tiny,fill=none]")

draw(x,y)

for i in [0,n-1] :
    #print(r"\draw [orange] (",x[i]-.5,r",",y[i]-.5*a[i],r") --  (",x[i]+.5,r",",y[i]+.5*a[i],r");")
    print(r"\fill [green!50!black] (",x[i],r",",y[i],r") circle [radius=3pt];")

print(r"\end{tikzpicture}")
print(r"\medskip")
print(r"\begin{tikzpicture}")
print(r"\tikzset{node style/.style = {inner sep = 2pt, outer sep = 2pt, fill = none}}")
print(r"\tkzTabInit [lgt=2.5,espcl=1.5] {$x$ / .5 ,variations\\de $f$ /1.5 }{",end="")
for i in range(n):
    print(r"$",x[i],r"$",sep="",end="")
    if i < n-1 :
        print(r",",end="")
print(r"}")
print(r"\tkzTabVar{",end="")
m = m0
for i in range(n) :
    print(chr(44+m)+"/"+r"$",y[i],r"$",sep="",end="")
    m = -m
    if i < n-1 :
        print(r",",end="")
print(r"}")
print(r"\end{tikzpicture}")
print(r"\end{center}")

finRflash()
