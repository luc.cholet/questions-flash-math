if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash
    
    settings=[]

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#---- fiche question
from random import *
from sympy import *

x = symbols('x')

x1 = x2 = choice(list(range(1,5)))*choice([-1,1])
while x1==x2:
    x2 = choice(list(range(1,5)))*choice([-1,1])
    
y1 = x1
while y1==x1:
    y1 = choice(list(range(-9,10)))
    
y2 = x2
while y2==x2 or y1==y2:
    y2 = choice(list(range(-9,10)))

    
if 'choix' in settings :
    choix = settings['choix']%2
else:
    choix = choice([0,1])
    
m = Rational(y2-y1,x2-x1)
p = y1-m*x1

def s(x,signe=False,parenthèses=False):
    S=str(x)
    if signe and x>0:
       S="+"+S
    if parenthèses and (signe or x<0):
        S="("+S+")"
    return S

#---- fiche question
debutQflash(r"Déterminer une forme affine")
if choix==0:
    print(r"Soit $f$ une fonction affine telle que")
    print(r"\[ f(",x1,")=",y1,r"\quad \text{et} \quad f(",x2,")=",y2,r"\]")
    print(r"Déterminer l'expression de $f(x)$, pour tout réel $x$")
else:
    print(r"Soit une droite $d$ passant par les points")
    print(r"\[ A(",x1,";",y1,r") \quad \text{et} \quad B(",x2,";",y2,r")\]")
    print(r"Déterminer une équation réduite de la droite $d$")
finQflash()

#---- fiche réponse
debutRflash()
if choix==0:
    print(r"\[ f(",x1,")=",y1,r"\quad \text{et} \quad f(",x2,")=",y2,r"\]")
    print(r"$f$ est affine donc $f \colon x \mapsto m\,x+p$\\")
else:
    print(r"\[ A(",x1,";",y1,r") \quad \text{et} \quad B(",x2,";",y2,r")\]")
    print(r"Comme $x_A \neq x_B$, il existe deux réels m et p tels que $d\colon y=m\,x+p$\\")
    
print(r"\begin{tikzpicture}[xscale=.8,yscale=.4,text height=1.5ex,text depth=.25ex]")
print(r"\draw (0,-2) grid [step=2] (8,2);")
print(r"\node at (1,1) {$x$};")
print(r"\node at (3,1) {$0$};")
print(r"\node at (5,1) {$",x1,r"$};")
print(r"\node at (7,1) {$",x2,r"$};")
if choix==0:
    print(r"\node at (1,-1) {$f(x)$};")
else:
    print(r"\node at (1,-1) {$y$};")
print(r"\node at (3,-1) {$p$};")
print(r"\node at (5,-1) {$",y1,r"$};")
print(r"\node at (7,-1) {$",y2,r"$};")

#print(r"\begin{scope}[color=orange, thick]")
print(r"\draw [-latex, color=orange, thick]  (5.1,2.1) to [bend left=60] (7,2.1);")
print(r"\node [shape=circle, draw=orange, text=orange, fill=orange!10!white, inner sep=1pt, thick] at (6,3.2) {$"+s(x2-x1,signe=True)+r"$}; ")
print(r"\draw [-latex, color=orange, thick]  (4.9,2.1) to [bend right=60] node [above] {$"+s(-x1,signe=True)+r"$} (3,2.1); ")

print(r"\draw [-latex, color=orange, thick]  (5.1,-2.1) to [bend right=60] (7,-2.1);")
print(r"\node [shape=circle, draw=orange, text=orange, fill=orange!10!white, inner sep=1pt, thick] at (6,-3.2) {$"+s(y2-y1,signe=True)+r"$};")
print(r"\draw [orange, thick, -latex]  (4.9,-2.1) to [bend left=60] node [below] {$"+s(-x1,signe=True)+r"\times m$} (3,-2.1);")

#print(r"\end{scope}")
print(r"\draw [red, -latex, thick] (7.5,3.2) to [bend left=40] node [fill=red!10!white, shape=circle,draw, inner sep=1pt] {$\times m$} (7.5,-3.2);")
print(r"\end{tikzpicture}\\")
print()
if choix==0:
    print(r"On en déduit $\displaystyle m=\frac{f(",x2,r")-f(",x1,")}{",x2,"-",s(x1,parenthèses=True),r"}=\frac{",y2,s(-y1,signe=True),r"}{",x2,s(-x1,signe=True),r"}="+latex(m)+r"$\\")
    print(r"de plus $\displaystyle p=f(0)=f(",x1,")"+s(-x1,signe=True)+r"\times m = "+latex(p)+r"$\\")
else:
    print(r"On en déduit $\displaystyle m=\frac{y_B-y_A}{x_B-x_A}=\frac{",y2,s(-y1,signe=True),r"}{",x2,s(-x1,signe=True),r"}="+latex(m)+r"$\\")
    print(r"or $y_A = m\times x_A +p$\\") 
    print(r"d'où $\displaystyle p=y_A-x_A\times m=",y1,s(-x1,signe=True),r"\times m = "+latex(p)+r"$\\")

print()

if choix==0:
    print(r"Conclusion, pour tout réel $x$, $f(x)=",latex(m*x+p),r"$, pour tout réel $x$")
else:
    print(r"Conclusion, $d \colon y=",latex(m*x+p),r"$")
finRflash()
