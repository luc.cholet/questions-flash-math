if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(title=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")
#----------
from random import *

m0 = choice([-1,+1])
m = m0

x = [randint(-4,-1)]
y = [randint(-3,3)]

for i in range(2):
    x.append(x[-1]+randint(3,4))
    y.append(y[-1]+randint(1,3)*m)
    m = -m
    
n = len(x)
    
#---- fiche question
debutQflash(r"Ordre et variations")

print(r"Soit une fonction $f$ dont le tableau de variations est")
print(r"\begin{center}")
print(r"\begin{tikzpicture}")
print(r"\tikzset{node style/.style = {inner sep = 2pt, outer sep = 2pt, fill = none}}")
print(r"\tkzTabInit [lgt=2.5,espcl=1.5] {$x$ / .5 ,variations\\de $f$ /1.5 }{",end="")
for i in range(n):
    print(r"$",x[i],r"$",sep="",end="")
    if i < n-1 :
        print(r",",end="")
print(r"}")
print(r"\tkzTabVar{",end="")
m = m0
for i in range(n) :
    print(chr(44+m)+"/"+r"$",y[i],r"$",sep="",end="")
    m = -m
    if i < n-1 :
        print(r",",end="")
print(r"}")
print(r"\end{tikzpicture}")
print(r"\end{center}")

i0 = randrange(2)
x1 = randint(x[i0],x[i0+1]-1)
eps = sample(range(1,10),2)
x1,x2 = (10*x1+eps[0])/10,(10*x1+eps[1])/10

sx1 = str(x1).replace('.',',')
sx2 = str(x2).replace('.',',')
sy1 = "f("+sx1+")"
sy2 = "f("+sx2+")"

print(r"Comparer $"+sy1+r"$ et $"+sy2+r"$.")

finQflash()

#---- fiche réponse
debutRflash()

print(r"\begin{center}")
print(r"\begin{tikzpicture}")
print(r"\tikzset{node style/.style = {inner sep = 2pt, outer sep = 2pt, fill = none}}")
print(r"\tkzTabInit [lgt=2.5,espcl=3] {$x$ / .5 ,variations\\de $f$ /2 }{",end="")
for i in range(n):
    print(r"$",x[i],r"$",sep="",end="")
    if i < n-1 :
        print(r",",end="")
print(r"}")
print(r"\tkzTabVar{",end="")
m = m0
for i in range(n) :
    print(chr(44+m)+"/"+r"$",y[i],r"$",sep="",end="")
    m = -m
    if i < n-1 :
        print(r",",end="")
print(r"}")
if x1<x2 :
    p1,p2 = 0.33,0.66
else :
    p1,p2 = 0.66,0.33
print(r"\tkzTabSetup[backgroundcolor = orange!20]")
print(r"\tkzTabVal[draw]{",i0+1,"}{",i0+2,"}{",p1,r"}{\color{red}$\scriptscriptstyle ",str(x1).replace('.',','),r"$}{\color{red}$\scriptscriptstyle f(",str(x1).replace('.',','),")$}",sep='')
print(r"\tkzTabVal[draw]{",i0+1,"}{",i0+2,"}{",p2,r"}{\color{red}$\scriptscriptstyle ",str(x2).replace('.',','),r"$}{\color{red}$\scriptscriptstyle f(",str(x2).replace('.',','),")$}",sep='')
print(r"\end{tikzpicture}")
print(r"\end{center}")

print(r"On a $",end="")
if x1<x2 :
    print(x[i0],"<",end='')
    print(r"\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=teal,text=black,inner sep=2pt] (X) {$",end='')
    print(sx1+"<"+sx2,end='')
    print(r"$};")
    print("<",x[i0+1],r"$\\")
else :
    print(x[i0+1],">",end='')
    print(r"\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=teal,text=black,inner sep=2pt] (X) {$",end='')
    print(sx1+">"+sx2,end='')
    print(r"$};")
    print(">",x[i0],r"$\\")
print(r"or sur l'intervalle $[\,",x[i0],r"\,;\,",x[i0+1],r"\,]$ la fonction~$f$ est ",sep='',end='')
if y[i0]<y[i0+1] :
    print(r"croissante donc elle conserve l'ordre\\")
    print("d'où ",end='')
    if x1<x2 :
        print(r"$f(",x[i0],")=",y[i0],"<",end='')
        print(r"\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$",end='')
        print(sy1,"<",sy2,end='')
        print(r"$};",end='')
        print("<",y[i0+1],"=f(",x[i0+1],r")$\\")
    else :
        print(r"$f(",x[i0+1],")=",y[i0+1],">",end='')
        print(r"\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$",end='')
        print(sy1,">",sy2,end='')
        print(r"$};",end='')
        print(">",y[i0],"=f(",x[i0],r")$\\")
else :
    print(r"décroissante donc elle change l'ordre\\")
    print("d'où ",end='')
    if x1<x2 :
        print(r"$f(",x[i0],")=",y[i0],">",end='')
        print(r"\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$",end='')
        print(sy1,">",sy2,end='')
        print(r"$};",end='')
        print(">",y[i0+1],"=f(",x[i0+1],r")$\\")
    else :
        print(r"$f(",x[i0+1],")=",y[i0+1],"<",end='')
        print(r"\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$",end='')
        print(sy1,"<",sy2,end='')
        print(r"$};",end='')
        print("<",y[i0],"=f(",x[i0],r")$\\")

finRflash()
