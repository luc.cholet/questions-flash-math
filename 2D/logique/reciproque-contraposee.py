#----
#
# Les "columns" sont intégrées à la class beamer
#
# \usepackage[tcolorbox]
#
#----
if __name__ == "__main__":
    settings = dict(q=1,rec=False)

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")


#----
from random import choice

lst = [
    [
        "Si un quadrilatère est un losange alors ses diagonales sont perpendiculaires.",
        "Si les diagonales d'un quadrilatère ne sont pas perpendiculaires alors ce quadrilatère n'est pas un losange.",
        "[V]",
        "Si les diagonales d'un quadrilatère sont perpendiculaires alors ce quadrilatère est un losange.",
        "Si un quadrilatère n'est pas un losange alors ses diagonales ne sont pas perpendiculaires.",
        r"[F], \begin{tikzpicture}[baseline] \draw [red,fill=red!20] (0,0) rectangle +(1ex,1ex); \draw [gray] (-1,0) -- ++(1,0) -- +(2,0) -- +(0,1) -- +(0,-1); \draw [thick] (-1,0) -- (0,1) -- (2,0) -- (0,-1) -- cycle; \end{tikzpicture}"
    ],
    [
        r"Si un parallélogramme a ses diagonales de même longueur alors ce parallélogramme est un rectangle.",
        r"Si un parallélogramme n'est un rectangle, alors ses diagonales n'ont pas mêmes longueurs.",
        "[V]",
        r"Si un parallélogramme est un rectangle, alors ses diagonales ont même longueur.",
        r"Si un parallélogramme n'a pas des diagonales de même longueur, alors ce parallélogramme n'est pas un rectangle.",
        "[V]."
    ],
    [
        "Si $n$ est un entier pair alors $n$ est divisible par $4$.",
        "Si un entier $n$ n'est pas divisible par $4$, alors $n$ n'est pas pair.",
        "[F], contre-exemple $n=6$.",
        "Si un entier $n$ est divisible par $4$ alors $n$ est pair.",
        "Si un entier $n$ n'est pas pair, alors $n$ n'est pas divisible par~4.",
        "[V]"
    ],
    [
        r"Si une fonction $f$ est impaire, alors $f(-5)=-f(5)$.",
        r"Si une fonction $f$ vérifie $f(-5)\neq -f(5)$, alors la fonction $f$ n'est pas impaire.",
        "[V]",
        r"Si une fonction $f$ vérifie $f(-5)=-f(5)$, alors la fonction $f$ est impaire.",
        r"Si une fonction $f$ n'est pas impaire, alors $f(-5)\neq -f(5)$.",
        "[F], il faut penser aux autres valeurs de $x$."
    ],
    [
        r"Si une fonction $f$ est paire, alors $f(-5)=f(5)$.",
        r"Si une fonction $f$ vérifie $f(-5)\neq f(5)$, alors la fonction $f$ n'est pas paire.",
        "[V]",
        r"Si une fonction $f$ vérifie $f(-5)=f(5)$, alors la fonction $f$ est paire.",
        r"Si une fonction $f$ n'est pas paire, alors $f(-5)\neq f(5)$.",
        "[F], il faut penser aux autres valeurs de $x$."
    ]
]

sVRAI = r"{\color{green!50!black} \textsc{Vraie}}"
sFAUX = r"{\color{red} \textsc{Fausse}}"

tcbset = r"\tcbset{colback=gray!20!white,colframe=black!75!white,fonttitle=\bfseries,boxsep=0pt,arc=2pt}"

if settings != None and 'q' in settings :
    prop = lst[settings['q'] % len(lst)]
else :
    prop = choice(lst)
    
if settings != None and 'rec' in settings :
    rec = settings['rec']
else :
    rec = choice([True, False])
    
if rec:
    Q = "Logique, réciproque"
    p1 = choice([0,3])
    p2 = 3-p1
else:
    Q = "Logique, contraposée"
    p1 = choice([0,3])
    p2 = p1+1
    

#---- fiche question
debutQflash(Q)
print(tcbset)
print(r"\begin{tcolorbox}[title=Proposition]")
print(prop[p1])
print(r"\end{tcolorbox}")
if rec:
    print(r"Quelle est la proposition réciproque ?\\")
else:
    print(r"Quelle est la contraposée de cette proposition?\\")
print(r"Ces deux propositions, sont-elles vraies (ou fausses) ?")
finQflash()

#---- fiche réponse
debutRflash()
print(tcbset)
print(r"\begin{minipage}{.47\textwidth}")
print(r"\begin{tcolorbox}[title=Proposition]")
print(prop[p1])
print(r"\end{tcolorbox}")
if rec:
    print(prop[(p1//3)*3+2].replace('[V]',sVRAI).replace('[F]',sFAUX)+r"\\")
print(r"\end{minipage} \hfill")
print(r"\begin{minipage}{.47\textwidth}")
if rec:
    print(r"\begin{tcolorbox}[title=Réciproque]")
else:
    print(r"\begin{tcolorbox}[title=Contraposée]")
print(prop[p2])
print(r"\end{tcolorbox}")
if rec:
    print(prop[(p2//3)*3+2].replace('[V]',sVRAI).replace('[F]',sFAUX)+r"\\")
print(r"\end{minipage}\\")
if not rec:
    R = prop[p1+2].replace('[V]',sVRAI).replace('[F]',sFAUX)
    print("Ces deux propositions sont "+R.replace('Vraie','vraies').replace('Fausse','fausses')+r"\\")
finRflash()
