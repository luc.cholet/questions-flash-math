#----
#
# \usepackage[xcolor] ou tikz qui l'utilise
#
#----
if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%fin question")


#----
from random import choice

lst = ["Pierre est triste [car] c'est la fin des vacances.",
       "C'est la fin des vacances [donc] Pierre est triste.",
       "Il pleut [donc] il y a des nuages.",
       "L'entier $n$ est pair [car] $n$ est divisible par $4$.",
       "L'entier $n$ est divisible par $4$ [donc] $n$ est pair.",
       "L'entier $n$ est pair [ssi] $n$ est divisible par $2$.",
       "Ce quadrilatère est un losange [donc] ses diagonales se coupent en leur milieu.",
       "Ce quadrilatère est un carré [donc] il a quatre angles droits.",
       "Ce quadrilatère est un rectangle [ssi] il a quatre angles droits.",
       "L'entier $n$ est divisible par $8$ [donc] $n$ est divisible par $2$ et par $4$.",
       "L'entier $n$ est divisible par $6$ [ssi] $n$ est divisible par $2$ et par $3$.",
       "La fonction $f$ est paire [donc] $f(5)=f(-5)$.",
       "$f(5)=f(-5)$ [car] la fonction $f$ est paire.",
       "La fonction $f$ est impaire [donc] $f(5)=-f(-5)$.",
       "$f(5)=-f(-5)$ [car] la fonction $f$ est impaire.",
       r"$x \in [\,0\,;\,5\,]$ [car] $0<x<5$",
       r"$0<x<5$ [donc] $x \in [\,0\,;\,5\,]$",
       r"$0\leq x \leq 5$ [ssi] $x \in [\,0\,;\,5\,]$"
]

s = choice(lst)

#---- fiche question
debutQflash("Logique, donc ou car ?")
print(r"Quelle est l'articulation logique qui convient ?\\")
print(r"(~\emph{donc}, \emph{car} ou \emph{si ou seulement si}~)\\")
print(r"\bigskip")
print(s.replace('[donc]','...').replace('[car]','...').replace('[ssi]','...')+r'\\')
finQflash()

#---- fiche réponse
debutRflash()
test = '[ssi]' in s
s = s.replace('[donc]', r"{\color{red}donc}")
s = s.replace('[car]', r"{\color{red}car}")
s = s.replace('[ssi]', r"{\color{red}si et seulement si}")
print(s)
if test:
    print(r"\newline")
    print(r"(On peut mettre aussi bien \emph{donc} que \emph{car})")
finRflash()