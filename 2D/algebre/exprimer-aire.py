if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("%---- fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("%---- fin question")
        
#---- fiche question
from sympy import *
from random import *

x = symbols(choice(['x','a','b','c']))

kMin, kMax = 2,1
while kMin>kMax or kMax<.1:
    a = randint(1,3)
    b = a
    while b==a :
        b = randint(2,10)
    
    c = randint(1,2)
    if random()<.1 :
        c = 0
    d = c
    while d==c :
        if c!=0 :
            d = randint(0,3)
        else:
            d = randint(2,max(3,b//2))
    
    limX = 10
    limY = 6
    
    ch = randrange(3)
    L = [a*x+b, a*x-b, b-a*x][ch]
    maxH = [lambda k: a*k+b, lambda k:a*k, lambda k:b][ch]
    eps, ligne1, ligne2 = [ (-.5, [x]*a+[b], []), (.5, [x]*a, [b]), (.5, [b], [x]*a) ][ch]
    
    kMin = 0
    if ch == 1:
        kMin=(b+1)/a
        
    kMax = b*0.7
    if ch == 2:
        kMax=(b-1)/a

    ch2 = randrange(3)
    if c==0 or d==0:
        ch2 = 0
    l = [c*x+d, c*x-d, d-c*x][ch2]
    maxV = [lambda k: c*k+d, lambda k:c*k, lambda k:d][ch2]
    eta, col1, col2 = [ (.5, [x]*c+[d], []), (-.5, [x]*c, [d]), (-.5, [d], [x]*c) ][ch2]
    
    if ch2 == 1:
        kMin=max(kMin,(d+1)/c)
    if ch2 == 2:
        kMax=min(kMax,(d-1)/c)

k = float((kMin+3*kMax)/4)

L1 = min(limX/maxH(k),limY/maxV(k))

debutQflash("Trouver une expression")
print(r"Exprimer les longueurs manquantes en fonction de $"+latex(x)+r"$, où $"+latex(x)+r"$ est une longueur inconnue exprimée en centimètres.\\")
print(r"Puis donner l'expression de l'aire $A$. (Expression développée.)\\")
print(r"\medskip")

print(r"\begin{tikzpicture}[scale=.75,thick]")
x0 = 0
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")
for e in ligne1 :
    if isinstance(e,int) :
        x1 = x0+e*L1
        s = str(e)+"~cm"
    else :
        x1 = x0+k*L1
        s = "$"+str(e)+"$"
    print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
    print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
    x0 = x1
eps = -eps
xMax = x0
print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
for e in ligne2 :
    if isinstance(e,int) :
        x1 = x0-e*L1
        s = str(e)+"~cm"
    else :
        x1 = x0-k*L1
        s = "$"+str(e)+"$"
    print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
    print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
    x0 = x1
X = x0
print(r"\draw [latex-latex,orange] ("+str(x0)+","+str(1+eps)+r") -- node [above] {?} (0,"+str(1+eps)+");")
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")
eps = -eps

y0 = 0
print(r"\draw [gray, thin] ("+str(-1.5+eta)+",0) -- ("+str(-.5+eta)+",0);")
for e in col1 :
    if e!=0:
        if isinstance(e,int) :
                y1 = y0-e*L1
                s = str(e)+"~cm"
        else :
            y1 = y0-k*L1
            s = "$"+str(e)+"$"
        print(r"\draw [latex-latex] ("+str(-1+eta)+","+str(y0)+") -- node [above,rotate=90] {"+s+"}  ("+str(-1+eta)+","+str(y1)+");")
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+","+str(y1)+") -- ("+str(-.5+eta)+","+str(y1)+");")
    y0 = y1
yMin = y0
Y = y0
if c!=0 :
    eta = -eta
    if not (c==1 and d==0) :
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+","+str(y1)+") -- ("+str(-.5+eta)+","+str(y1)+");")
    for e in col2 :
        if isinstance(e,int) :
            y1 = y0+e*L1
            s = str(e)+"~cm"
        else :
            y1 = y0+k*L1
            s = "$"+str(e)+"$"
        print(r"\draw [latex-latex] ("+str(-1+eta)+","+str(y0)+") -- node [above,rotate=90] {"+s+"}  ("+str(-1+eta)+","+str(y1)+");")
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+","+str(y1)+") -- ("+str(-.5+eta)+","+str(y1)+");")
    y0 = y1
    Y = y0
    if not (c==1 and d==0) :
        print(r"\draw [latex-latex,orange] ("+str(-1+eta)+","+str(y0)+r") -- node [above,rotate=90] {?} ("+str(-1+eta)+",0);")
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+",0) -- ("+str(-.5+eta)+",0);")
print(r"\filldraw [fill=gray!50] (0,0) rectangle ("+str(xMax)+","+str(yMin)+");")
print(r"\filldraw [fill=orange!20!white,draw=orange] (0,0) rectangle node {\color{orange} $A$} ("+str(X)+","+str(Y)+");")
print(r"\end{tikzpicture}\\")
eta = -eta

finQflash()

#---- fiche réponse
debutRflash()
print(r"\begin{tikzpicture}[scale=.8,thick]")
x0 = 0
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")
for e in ligne1 :
    if isinstance(e,int) :
        x1 = x0+e*L1
        s = str(e)+"~cm"
    else :
        x1 = x0+k*L1
        s = "$"+str(e)+"$"
    print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
    print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
    x0 = x1
eps = -eps
xMax = x0
print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
for e in ligne2 :
    if isinstance(e,int) :
        x1 = x0-e*L1
        s = str(e)+"~cm"
    else :
        x1 = x0-k*L1
        s = "$"+str(e)+"$"
    print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
    print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
    x0 = x1
X = x0
print(r"\draw [latex-latex,orange] ("+str(x0)+","+str(1+eps)+r") -- node [above] {$"+latex(L)+"$} (0,"+str(1+eps)+");")
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")
eps = -eps

y0 = 0
if (c==0) :
    eta = -eta
print(r"\draw [gray, thin] ("+str(-1.5+eta)+",0) -- ("+str(-.5+eta)+",0);")
for e in col1 :
    if e!=0:
        if isinstance(e,int) :
                y1 = y0-e*L1
                s = str(e)+"~cm"
        else :
            y1 = y0-k*L1
            s = "$"+str(e)+"$"
        print(r"\draw [latex-latex] ("+str(-1+eta)+","+str(y0)+") -- node [above,rotate=90] {"+s+"}  ("+str(-1+eta)+","+str(y1)+");")
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+","+str(y1)+") -- ("+str(-.5+eta)+","+str(y1)+");")
    y0 = y1
yMin = y0
Y = y0
if c!=0 :
    eta = -eta
    if not (c==1 and d==0) :
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+","+str(y1)+") -- ("+str(-.5+eta)+","+str(y1)+");")
    for e in col2 :
        if isinstance(e,int) :
            y1 = y0+e*L1
            s = str(e)+"~cm"
        else :
            y1 = y0+k*L1
            s = "$"+str(e)+"$"
        print(r"\draw [latex-latex] ("+str(-1+eta)+","+str(y0)+") -- node [above,rotate=90] {"+s+"}  ("+str(-1+eta)+","+str(y1)+");")
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+","+str(y1)+") -- ("+str(-.5+eta)+","+str(y1)+");")
        y0 = y1
    Y = y0
    if not (c==1 and d==0) :
        print(r"\draw [latex-latex,orange] ("+str(-1+eta)+","+str(y0)+r") -- node [above,rotate=90] {$"+latex(l)+"$} ("+str(-1+eta)+",0);")
        print(r"\draw [gray, thin] ("+str(-1.5+eta)+",0) -- ("+str(-.5+eta)+",0);")
print(r"\filldraw [fill=gray!50] (0,0) rectangle ("+str(xMax)+","+str(yMin)+");")
print(r"\filldraw [fill=orange!20!white,draw=orange] (0,0) rectangle node {\color{orange} $A$} ("+str(X)+","+str(Y)+");")
print(r"\end{tikzpicture}\\")

print(r"\[A=",end='')
if c*d==0 :
    print(latex(l),end='')
else:
    print("("+latex(l)+")",end='')
print("\,("+latex(L)+r")="+latex(expand(L*l))+r"\]")

finRflash()