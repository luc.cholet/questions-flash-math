if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
#----------
from random import *

def plusmoins(b):
    if b>0 :
        return "+"+str(b)
    elif b<0 :
        return str(b)
    else :
        return ""
        
def plusmoinsax(a):
    if a==1 :
        return "+x"
    elif a==-1 :
        return "-x"
    elif a>0 :
        return "+"+str(a)+r"\,x"
    elif a<0 :
        return str(a)+r"\,x"
    else :
        return ""

def termeax(a):
    if a==1 :
        return "x"
    elif a==-1 :
        return "-x"
    elif a>0 :
        return str(a)+r"\,x"
    elif a<0 :
        return str(a)+r"\,x"
    else :
        return ""
        
def PGCD(a,b):
    if a%b==0 :
        return b
    else :
        return PGCD(b,a%b)
        
def pfrac(a,b):
    if a==0 :
        return "0"
    if b<0 :
        a,b = -a, -b
    d=PGCD(abs(a),b)
    a,b = a//d,b//d
    if b==1 :
        return a
    else :
        if a>0:
            return r"\dfrac{"+str(a)+r"}{"+str(b)+r"}"
        else :
            return r"-\dfrac{"+str(-a)+r"}{"+str(b)+r"}"

#---- fiche question
debutQflash("Équation")
a=randint(1,5)*choice([-1,1])
b=randint(1,9)*choice([-1,1])
c=a+randint(1,5)*choice([-1,1])
d=randint(1,9)*choice([-1,1])


print(r"Résoudre l'équation")
if c!=0 :
    print(r"\[",termeax(a),plusmoins(b),r"=",termeax(c),plusmoins(d),r"\]")
else :
    print(r"\[",termeax(a),plusmoins(b),r"=",d,r"\]")
finQflash()

#---- fiche réponse
debutRflash()

if c!=0 :
    print("$",termeax(a),plusmoins(b),r"=\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$",termeax(c),r"$};",plusmoins(d),r"$\\")
    print("ssi $",termeax(a),plusmoins(b),r"{\color{red}",plusmoinsax(-c),"}=",termeax(c),plusmoins(d),r"{\color{red}",plusmoinsax(-c),r"}$\\")
    print("ssi ",end="")
print(r"$",termeax(a-c),r"\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=teal,text=black,inner sep=2pt] (X) {$",plusmoins(b),r"$};=",d,r"$\\")
print("ssi $",termeax(a-c),plusmoins(b),r"{\color{teal}",plusmoins(-b),r"}=",d,r"{\color{teal}",plusmoins(-b),r"}$\\")
if a-c==1 :
    print("ssi $x=",d-b,r"$\\")
elif a-c==-1 :
    print(r"ssi $\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=orange,text=black,inner sep=2pt] (X) {$-$};\,x=",d-b,r"$\\")
    if d-b>=0 :
        print(r"ssi $x={\color{orange}-}",d-b,r"$\\")
    else :
        print(r"ssi $x={\color{orange}-}(",d-b,r")$\\")
else :
    print(r"ssi $\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=orange,text=black,inner sep=2pt] (X) {$",(a-c),r"$};\,x=",d-b,r"$\\")
    print(r"ssi $x=\bgroup \color{orange} \dfrac{\color{black}",d-b,r"}{",a-c,r"}\egroup$\\")
print(r"\medskip")
print(r"d'où l'ensemble solution $\mathcal{S}=\left\{",pfrac(d-b,a-c),r"\right\}$\\")

finRflash()
print()