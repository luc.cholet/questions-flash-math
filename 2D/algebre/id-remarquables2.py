if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
        
#---- fiche question
from random import *
from sympy import *

x = symbols('x')

lst = sample([1,1,2,3,4,5,6,7,8,9],2)
a = lst[0]*x
b = lst[1]

c = randrange(3) # on choisit une des trois identités remarquables
e = [(a+b)**2, (a-b)**2, (a-b)*(a+b)][c]

egalité = choice( [ (0, [latex(a**2),"\,+\,",latex(2*a*b),"\,+\,",latex(b**2)], 0b1111, 0b10),
                    (0, [latex(a**2),"\,+\,",latex(2*a*b),"\,+\,",latex(b**2)], 0b11011, 0b10),
                    (1, [latex(a**2),"\,-\,",latex(2*a*b),"\,+\,",latex(b**2)], 0b1111, 0b10),
                    (1, [latex(a**2),"\,-\,",latex(2*a*b),"\,+\,",latex(b**2)], 0b11011, 0b10),
                    (2, [latex(a**2),"\,-\,",latex(b**2)], 0b110, 0b011),
                    (2, [latex(a**2),"\,-\,",latex(b**2)], 0b11, 0b110)] )

fDev = ["a^2+2ab+b^2", "a^2-2ab+b^2", "a^2-b^2"]
fFac = ["(a+b)^2", "(a-b)^2", "(a-b)\,(a+b)"]

c, lst, trouesDef, trouesFac = egalité

eDev = ""
n = 1
for s in lst :
    if n&trouesDef :
        eDev += r"\bgroup \color{black} "+s+r"\egroup"
    else :
        eDev += r"\bgroup \color{red} ? \egroup"
    n *= 2

if trouesFac&1 :
    somme = r"\bgroup \color{black} "+latex(a)+r"\egroup"
    diff = somme
else :
    somme = r"\bgroup \color{red} ? \egroup"
    diff = somme
    
if trouesFac&2 :
    somme += r"\bgroup \color{black} \,+\, \egroup"
    diff +=  r"\bgroup \color{black} \,-\, \egroup"
else :
    somme += r"\bgroup \color{red} \,\cdots\, \egroup"
    diff += r"\bgroup \color{red} \,\cdots\, \egroup"
    
if trouesFac&4 :
    somme += r"\bgroup \color{black} "+latex(b)+r"\egroup"
    diff += r"\bgroup \color{black} "+latex(b)+r"\egroup"
else :
    somme += r"\bgroup \color{red} ? \egroup"
    diff += r"\bgroup \color{red} ? \egroup"

eFac = [ "("+somme+")^2", "("+diff+")^2", "("+diff+r")\,("+somme+")"][c]

debutQflash("Identités remarquables")

print(r"On considère l'égalité suivante dont certains termes ont été cachés")

print(r"\["+eDev+"="+eFac+r"\]")
print(r"Quelle est l'identité remarquable cachée derrière cette égalité ?")

finQflash()

#---- fiche réponse
debutRflash()
print(r"Il fallait trouver")

eDev = ""
n = 1
for s in lst :
    if n&trouesDef :
        eDev += r"\bgroup \color{black} "+s+r"\egroup"
    else :
        eDev += r"\bgroup \color{red}"+s+r"\egroup"
    n *= 2

col = ["red", "black"][trouesFac&1]
somme = r"\bgroup \color{"+col+"} "+latex(a)+r"\egroup"
diff = somme

trouesFac >>= 1
col = ["red", "black"][trouesFac&1]
somme += r"\bgroup \color{"+col+"} \,+\, \egroup"
diff +=  r"\bgroup \color{"+col+"} \,-\, \egroup"

trouesFac >>= 1
col = ["red", "black"][trouesFac&1]
somme += r"\bgroup \color{"+col+"} "+latex(b)+r"\egroup"
diff += r"\bgroup \color{"+col+"} "+latex(b)+r"\egroup"

eFac = [ r"\begingroup \color{black}("+somme+r")^2 \endgroup", r"\begingroup \color{black}("+diff+r")^2 \endgroup", r"\begingroup \color{black}("+diff+r")\,("+somme+r") \endgroup"][c]

print(r"\[\begingroup \color{orange} \underbrace{"+eDev+r"}_{"+fDev[c]+r"""}
\endgroup
=
\begingroup \color{orange} \underbrace{"""+eFac+r"}_{"+fFac[c]+r"} \endgroup \]")
finRflash()
