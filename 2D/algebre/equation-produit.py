if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
#----------
from random import *

def plusmoins(b):
    if b>0 :
        return "+"+str(b)
    elif b<0 :
        return str(b)
    else :
        return ""
        
def plusmoinsax(a):
    if a==1 :
        return "+x"
    elif a==-1 :
        return "-x"
    elif a>0 :
        return "+"+str(a)+r"\,x"
    elif a<0 :
        return str(a)+r"\,x"
    else :
        return ""

def termeax(a):
    if a==1 :
        return "x"
    elif a==-1 :
        return "-x"
    elif a>0 :
        return str(a)+r"\,x"
    elif a<0 :
        return str(a)+r"\,x"
    else :
        return ""
        
def PGCD(a,b):
    if a%b==0 :
        return b
    else :
        return PGCD(b,a%b)
        
def pfrac(a,b):
    if a==0 :
        return "0"
    if b<0 :
        a,b = -a, -b
    d=PGCD(abs(a),b)
    a,b = a//d,b//d
    if b==1 :
        return a
    else :
        if a>0:
            return r"\dfrac{"+str(a)+r"}{"+str(b)+r"}"
        else :
            return r"-\dfrac{"+str(-a)+r"}{"+str(b)+r"}"

#---- fiche question
debutQflash("Équation")
a = randint(1,5)*choice([-1,1])
b = randint(0,9)*choice([-1,1])
c = randint(1,5)*choice([-1,1])
# pour avoir deux solutions -b/a ≠ -d/c soit d ≠ b*c/a
lst = list(range(-9,10))
lst.remove(0)
if (b*c)%a == 0 and (b*c)//a in lst :
    lst.remove((b*c)//a)
d = choice(lst)


print(r"Résoudre l'équation")
if b == 0:
    print(r"\[ ",termeax(a),r"\,(",termeax(c),plusmoins(d),r")=0\]")
else :
    print(r"\[ (",termeax(a),plusmoins(b),r")\,(",termeax(c),plusmoins(d),r")=0\]")

finQflash()

#---- fiche réponse
debutRflash()

if b == 0:
    print(r"\[ ",termeax(a),r"\,(",termeax(c),plusmoins(d),r")=0\]")
else :
    print(r"\[ (",termeax(a),plusmoins(b),r")\,(",termeax(c),plusmoins(d),r")=0\]")
print(r"or, un produit est nul si et seulement si l'un de ses facteurs est nul, donc\\")
print()
print(r"\qquad \begin{minipage}[t]{.45\linewidth}")
print("soit ",end='')
if b!=0 :
    print("$",termeax(a),r"\,\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=teal,text=black,inner sep=2pt] (X) {$",plusmoins(b),r"$};\,=0$\\")
    print(r"\rule{1em}{0pt} ssi $"+termeax(a)+plusmoins(b)+r"\,{\color{teal} "+plusmoins(-b)+"}\,=\,0\,{\color{teal}"+plusmoins(-b)+r"}$\\")
    print(r"\rule{1em}{0pt} ssi ",end='')
if abs(a)!=1 :
    print(r"$\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$",a,r"$};\,x = ",-b,r"$\\")
    print(r"\rule{1em}{0pt} ssi $x=\bgroup \color{red}\frac{ \color{black}",-b,r"}{",a,r"}\egroup$\\")
elif a==-1:
    print(r"$\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$-$};\,x = ",-b,r"$\\")
    print(r"\rule{1em}{0pt} ssi $x = {\color{red}-}")
    if b>0:
        print("(",-b,r")$\\")
    else:
        print(-b,r"$\\")
else :
    print("$x=",-b,r"$\\")
print(r"\end{minipage} \hfill")
print(r"\begin{minipage}[t]{.45\linewidth}")
print("soit ",end='')
print("$",termeax(c),r"\,\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=teal,text=black,inner sep=2pt] (X) {$",plusmoins(d),r"$};\,=0$\\")
print(r"\rule{1em}{0pt} ssi $"+termeax(c)+plusmoins(d)+r"\,{\color{teal} "+plusmoins(-d)+"}\,=\,0\,{\color{teal}"+plusmoins(-d)+r"}$\\")
print(r"\rule{1em}{0pt} ssi ",end='')
if abs(c)!=1 :
    print(r"$\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$",c,r"$};\,x = ",-d,r"$\\")
    print(r"\rule{1em}{0pt} ssi $x=\bgroup \color{red}\frac{ \color{black}",-d,r"}{",c,r"}\egroup$\\")
elif c==-1:
    print(r"$\tikz [baseline=(X.base)] \node [draw,thick,rectangle,rounded corners=2pt,color=red,text=black,inner sep=2pt] (X) {$-$};\,x = ",-d,r"$\\")
    print(r"\rule{1em}{0pt} ssi $x = {\color{red}-}")
    if d>0:
        print("(",-d,r")$\\")
    else:
        print(-d,r"$\\")
else :
    print("$x=",-d,r"$\\")
print(r"\end{minipage}\\")
print(r"\medskip")
if -b/a < -d/c :
    print(r"d'où l'ensemble solution $\mathcal{S}=\left\{\,",pfrac(-b,a),r"\,;\,",pfrac(-d,c),r"\,\right\}$\\")
else :
    print(r"d'où l'ensemble solution $\mathcal{S}=\left\{\,",pfrac(-d,c),r"\,;\,",pfrac(-b,a),r"\,\right\}$\\")
    

finRflash()
print()