if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
        
#---- fiche question
from random import *
from sympy import *

x = symbols('x')

lst = sample([1,1,2,3,4,5,6,7,8,9],2)
a = lst[0]*x
b = lst[1]

c = randrange(3) # on choisit une des trois identités remarquables
e = [(a+b)**2, (a-b)**2, (a-b)*(a+b)][c]

eFac = latex(e).replace('x',r"\,x")
eDev = latex(expand(e)).replace('x',r"\,x")
fFac = ["(a+b)^2", "(a-b)^2", "(a-b)\,(a+b)"][c]
fDev = ["a^2+2ab+b^2", "a^2-2ab+b^2", "a^2-b^2"][c]

c = choice([True,False]) # on donne soit la forme fact soit celle développée

debutQflash("Identités remarquables")

print(r"On considère l'expression")
if c :
    print(r"\["+eDev+r"\]")
else :
    print(r"\["+eFac+r"\]")
print(r"Cette expression est-elle factorisée ou développée ?\\")
print(r"Grâce aux identités remarquables, en déduire l'autre forme de cette expression.\\")

finQflash()

#---- fiche réponse
debutRflash()
if c :
    print(r"$"+eDev+r"$ est une forme développée.\\")
    print(r"On peut donc la factoriser")
    print(r"""\[\begingroup
\color{orange}
\underbrace{\bgroup \color{black}"""+eDev+r""" \egroup}_{"""+fDev+r"""}
\endgroup
=
\begingroup
\color{orange}
\underbrace{\bgroup \color{black}"""+eFac+r""" \egroup}_{"""+fFac+r"""}
\endgroup \]""")
else :
    print(r"$"+eFac+r"$ est une forme factorisée.\\")
    print(r"On peut donc la développer")
    print(r"""\[\begingroup
\color{orange}
\underbrace{\bgroup \color{black}"""+eFac+r""" \egroup}_{"""+fFac+r"""}
\endgroup
=
\begingroup
\color{orange}
\underbrace{\bgroup \color{black}"""+eDev+r""" \egroup}_{"""+fDev+r"""}
\endgroup \]""")

finRflash()
