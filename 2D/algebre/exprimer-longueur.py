if __name__ == "__main__":
    global debutQflash, finQflash, debutRflash, finRflash

    settings = None

    def debutQflash(titre=None):
        print("%---- début question")
        
    def finQflash():
        print("fin question")
    
    def debutRflash():
        print("%---- début réponse")
        
    def finRflash():
        print("fin question")
        
#---- fiche question
from sympy import *
from random import *

if settings!=None and 'lettre' in settings:
    x = symbols(settings['lettre'])
else :
    x = symbols(choice(['x','a','b','c']))

a = randint(1,3)
b = a
while b==a :
	b = randint(2,10)

if settings!=None and 'a' in settings:
    a = settings['a']
if settings!=None and 'b' in settings:
    b = settings['b']

k = b*.7
L1 = .8
limX = 10
limY = 6

if settings!=None and 'choix' in settings:
    c = settings['choix']
else :
    c = randrange(3)

L = [a*x+b, a*x-b, b-a*x][c]
eps, ligne1, ligne2 = [ (-.5, [x]*a+[b], []), (.5, [x]*a, [b]), (.5, [b], [x]*a) ][c]

if c==0 :
	L1 = limX/(a*k+b)

if c==1 :
	while a*k < b+2*a*k/limX :
		k *= 1.1
	L1 = limX/(a*k)
	
if c==2 :
	L1 = limX/b # 2<b<5
	# 2/L1 = 2*b/limX donc 2/L1 < b si limX>2
	while b<a*k+2/L1 : 
		k *= .9

debutQflash("Trouver une expression")
print(r"Exprimer la longueur manquante en fonction de $"+latex(x)+r"$, où $"+latex(x)+r"$ est une longueur inconnue exprimée en centimètres.\\")
print(r"\medskip")
print(r"\begin{tikzpicture}[thick]")
x0 = 0
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")
for e in ligne1 :
	if isinstance(e,int) :
		x1 = x0+e*L1
		s = str(e)+"~cm"
	else :
		x1 = x0+k*L1
		s = "$"+str(e)+"$"
	print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
	print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
	x0 = x1
eps = -eps
print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
for e in ligne2 :
	if isinstance(e,int) :
		x1 = x0-e*L1
		s = str(e)+"~cm"
	else :
		x1 = x0-k*L1
		s = "$"+str(e)+"$"
	print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
	print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
	x0 = x1
print(r"\draw [latex-latex,orange] ("+str(x0)+","+str(1+eps)+r") -- node [above] {?} (0,"+str(1+eps)+");")
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")
print(r"\end{tikzpicture}\\")
eps = -eps
finQflash()

#---- fiche réponse
debutRflash()
print(r"\begin{tikzpicture}[thick]")
x0 = 0
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")
for e in ligne1 :
	if isinstance(e,int) :
		x1 = x0+e*L1
		s = str(e)+"~cm"
	else :
		x1 = x0+k*L1
		s = "$"+str(e)+"$"
	print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
	print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
	x0 = x1
eps = -eps
print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
for e in ligne2 :
	if isinstance(e,int) :
		x1 = x0-e*L1
		s = str(e)+"~cm"
	else :
		x1 = x0-k*L1
		s = "$"+str(e)+"$"
	print(r"\draw [latex-latex] ("+str(x0)+","+str(1+eps)+") -- node [above] {"+s+"}  ("+str(x1)+","+str(1+eps)+");")
	print(r"\draw [gray, thin] ("+str(x1)+","+str(1.5+eps)+") -- ("+str(x1)+","+str(.5+eps)+");")
	x0 = x1
print(r"\draw [latex-latex,orange] ("+str(x0)+","+str(1+eps)+r") -- node [above] {$"+latex(L)+"$} (0,"+str(1+eps)+");")
print(r"\draw [gray, thin] (0,"+str(1.5+eps)+") -- (0,"+str(.5+eps)+");")

print(r"\end{tikzpicture}")
finRflash()